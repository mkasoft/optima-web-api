﻿using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Service
{
    public class OptimaTowaryException : Exception
    {

        public int ErrorNumber { get; set; }
        public TowaryKryteriaDto criteriaDto { get; set; }
        public  ZasobyKryteriaDto zasobyKryteriaDto { get; set; }

        
            
        public OptimaTowaryException()
        {
        }

        public OptimaTowaryException(string message)
            : base(message)
        {
        }


        public OptimaTowaryException(int _errorNumber, TowaryKryteriaDto _criteriaDto, string message, Exception inner)
     : base(message , inner)
        {
            ErrorNumber = _errorNumber;
            criteriaDto = _criteriaDto;
        }
        public OptimaTowaryException(int _errorNumber, ZasobyKryteriaDto _zasobyKryteriaDto, string message, Exception inner)
         : base(message , inner)
        {
            ErrorNumber = _errorNumber;
            zasobyKryteriaDto = _zasobyKryteriaDto;
        }

        public OptimaTowaryException(string message, Exception inner)
            : base(message, inner)
        {
        }
        public string GetPayloadError()
        {
            if (zasobyKryteriaDto != null)
            {
                return zasobyKryteriaDto.ToExceptionString();
            }
            else if (criteriaDto != null)
            {
                return criteriaDto.ToExceptionString();
            }

            return "";
        }
    }
}


