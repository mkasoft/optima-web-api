﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RestApi
{
    public class ApiKeyHandler : DelegatingHandler
    {
        public readonly string ApiKeyName = "apikey";
        private readonly IGetAllApiKeysQuery _apiKeysQuery;

        public ApiKeyHandler(IGetAllApiKeysQuery apiKeysQuery)
        {
            _apiKeysQuery = apiKeysQuery ?? throw new ArgumentNullException(nameof(apiKeysQuery));
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage,
            CancellationToken canellationTocken)
        {
            var path = httpRequestMessage.RequestUri.AbsolutePath;
            if (!path.StartsWith("/api/"))
            {
                return await base.SendAsync(httpRequestMessage, canellationTocken);
            }

            var validKey = false;
            var checkKeyExists = httpRequestMessage.Headers.TryGetValues(ApiKeyName, out var requestHeaders);
            if (checkKeyExists)
            {
                var keys = await _apiKeysQuery.ExecuteAsync();
                validKey = keys.ContainsKey(requestHeaders.FirstOrDefault());
            }

            if (!validKey)
            {
                return httpRequestMessage.CreateResponse(HttpStatusCode.Forbidden, "Invalid key");
            }

            return await base.SendAsync(httpRequestMessage, canellationTocken);
        }
    }
}