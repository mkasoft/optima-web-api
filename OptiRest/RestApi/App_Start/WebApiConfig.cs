﻿using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.Service;
using OptimaSDKLib.Base;
using Unity;
using Unity.AspNet.WebApi;
using Unity.Injection;

namespace RestApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new CustomExceptionFilter());

            var container = new UnityContainer();
            //container.RegisterType<IGetAllApiKeysQuery, InMemoryGetAllApiKeysQuery>();
            container.RegisterSingleton<OptimaSessionPool>();
            container.RegisterType<IDokumentyService, DokumentyService>();
            container.RegisterType<IDokumentCommand, DokumentCommand>();
            container.RegisterType<IKontrahenciCommand, KontrahenciCommand>();
            container.RegisterType<IKontrahenciService, KontrahenciService>();            
            container.RegisterType<ITowaryCommand, TowaryCommand>();
            container.RegisterType <ITowaryService, TowaryService>() ;
            container.RegisterType<IWydrukiCommand, WydrukiCommand>();
            container.RegisterType<IWydrukiService, WydrukiService>();


            config.DependencyResolver = new UnityDependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new ValidateModelAttribute());


            config.Formatters.Add(new BsonMediaTypeFormatter());

            // Convert all dates to UTC
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
        
        }

        
    }
}
