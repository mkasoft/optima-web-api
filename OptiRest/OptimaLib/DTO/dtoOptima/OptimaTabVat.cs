namespace com.asoft.OptimaLib.dtoOptima
{
    public class OptimaTabVat
    {
        public decimal TrV_StawkaPod{ get; set; }
        public decimal TrV_FlagaVat { get; set; }
        public decimal TrV_NettoR { get; set; }
        public decimal TrV_VatR { get; set; }
        public OptimaTabVat() { }
        public OptimaTabVat (decimal StawkaPod,decimal FlagaVat,decimal NettoR,decimal VatR)
        {
            this.TrV_StawkaPod = StawkaPod;
            this.TrV_FlagaVat = FlagaVat;
            this.TrV_NettoR = NettoR;
            this.TrV_VatR = VatR;
        }
                
    }
}