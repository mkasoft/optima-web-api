﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;
using System;
using System.Runtime.InteropServices;

namespace OptimaLibUnitTest
{
    [TestClass]
    public class SesjaTest
    {
        public SesjaTest()
        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();
        }

        [TestMethod]
        public void UtworzNowaSesje()
        {
            LogowanieCommand logowanie = null;
            try
            {
                const string user = "TEST";
                const string haslo = "test";
                const string firma = "TEST";
                //firma = "I29";

                ShowInfo(string.Format("Logowanie: {0} {1}", firma, user));

                logowanie = new LogowanieCommand();
                logowanie.Zaloguj(user, haslo, firma);


            }
            catch (COMException e)
            {
                ShowError(e);
            }
            catch (Exception e)
            {
                ShowError(e);
            }
            finally
            {
                if (logowanie != null) logowanie.Wyloguj();
            }
        
        }

        static void ShowInfo(string text)
        {
            System.Console.WriteLine(text);
        }
        void ShowError(Exception e)
        {
            System.Console.WriteLine("Błąd: " + ErrorInfo.Message(e));
        }

        [TestMethod]
        public void CzyUdaloSieUstawicSesjeOptimy()
        {

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.IsNotNull(sesja);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);

        }
        [TestMethod]
        public void DwieSesjeZPuli()
        {

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.IsNotNull(sesja);
            var sesja2 = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.IsNull(sesja2);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);

        }

        public void CzyWPuliJestTaSamaSesja()
        {

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.IsNotNull(sesja);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
            var sesja2 = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.AreEqual(sesja,sesja2);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja2);

        }
        [TestMethod]
        public void ReKonstrukcjaSessji()
        {

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            Assert.IsNotNull(sesja);
            var nowaSesja = com.asoft.OptimaLib.Service.OptimaSessionPool.RecreateNewSession(sesja);
            Assert.IsNotNull(nowaSesja);
            Assert.AreNotEqual(sesja, nowaSesja);

            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(nowaSesja);

        }
    }
}