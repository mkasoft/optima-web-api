﻿using System;

namespace com.asoft.OptimaLib.dtoService
{
    public class KontrahentDto
    {
        public long IdKthCnt { get; set; }
        public string Kod { get; set; }
        public string Nazwa1 { get; set; }
        public string Nazwa2 { get; set; }
        public string Nazwa3 { get; set; }
        public string Nip { get; set; }
        public string Regon { get; set; }
        public string KodPocztowy { get; set; }
        public string Miejscowosc { get; set; }
        public string Ulica { get; set; }
        public bool CzyDostawca { get; set; }
        public bool CzyOdbiorca { get; set; }
        public bool CzyArchiw { get; set; }
        public bool CzyWstrzTrans { get; set; }
        public string Fax { get; set; }
        public string Wojewodztwo { get; set; }
        public string Email { get; set; }
        public string Www { get; set; }
        public long FormaPlatId { get; set; }
        public long PlatTermin { get; set; }
        public string WskOpak { get; set; }
        public string Telefon { get; set; }
        public DateTime TimeStampModyfikacji { get; set; }
        public string NrDomu { get; set; }
        public string NrLokalu { get; set; }
        public string Dodatkowe { get; set; }
        public string Kraj { get; set; }
    }
}