﻿using System;
using System.Collections.Generic;

namespace com.asoft.OptimaLib.dtoService
{
    public class ImportDokumentDto
    {
 
       
        public string  ExportKod { get; set; }
      
        public Nullable<System.DateTime> DataDok { get; set; }
        public Nullable<System.DateTime> DataWys { get; set; }
        public Nullable<System.DateTime> DataSprzedazy { get; set; }

        public Nullable<System.Decimal> NrDok { get; set; }
        public string SeriaDok { get; set; }
        public string NumerObcy { get; set; }

        public int CzyZapisBufor { get; set; }

        public decimal Netto { get; set; }

        public decimal Vat { get; set; }

        public decimal Brutto { get; set; }

        public string MagazynZrodlowy { get; set; }
        public string MagazynDocelowy { get; set; }
        public decimal FormaPlatnosci { get; set; }

        public string RachunekAkronim { get;  set; }
        public Nullable<System.DateTime> TerminPlatosci { get;  set; }

        public string NumerPelny { get;  set; }
        public string Opis { get;  set; }
        public string KodPlatnika { get;  set; }
        public string KodDostawcy { get;  set; }
        public System.String KodOdbiorcy { get;  set; }
    
        public string NumerPelnyKorekty { get; set; }
        public short? CzyKorektaCeny { get;  set; }
        public int CzyKorekta { get;  set; }      


        public string Kategoria { get; set; }

        public int Rodzaj { get; set; }

        public int CzyPos { get;  set; }

        public List<ImportDokumentPozycjaDto> PozycjeDokumentuImportowanego { get;  set; }

   

        public override string ToString()
        {
            return "Dokument: " +  " TrN_Rodzaj= " + Rodzaj + " " + NumerPelny + " netto=" + Netto + " vat=" + Vat
                 + " brutto=" + Brutto + " MAGAZYN= " + MagazynZrodlowy + " Knt_Kod=" + KodPlatnika 
                 + " DateTime=" + DataWys;
        }

        public  string ToExceptionString()
        {
            return  "Dokument:"  + " TrN_Rodzaj= " + Rodzaj + " " + NumerPelny + " netto=" + Netto + " vat=" + Vat
                 + " brutto=" + Brutto + " MAGAZYN= " + MagazynZrodlowy + " Knt_Kod=" + KodPlatnika
                 + " DateTime=" + DataWys; 
        }
    }

}