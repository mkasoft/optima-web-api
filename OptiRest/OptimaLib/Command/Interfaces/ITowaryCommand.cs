﻿using CDNBase;
using CDNHlmn;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Base
{
    public interface ITowaryCommand
    {
        List<TowaryDto> ListaTowarow(CDNBase.AdoSession sesja, TowaryKryteriaDto towaryKryteriaDto);
        List<ZasobyDto> ListaZasobow(AdoSession sesja, ZasobyKryteriaDto zasobyKryteriaDto);
    }
}
