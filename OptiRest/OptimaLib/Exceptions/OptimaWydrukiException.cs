﻿using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Service
{
    public class OptimaWydrukiException : Exception
    {
        public int ErrorNumber { get; set; }
        public OptimaWydrukiException()
        {
        }

        public OptimaWydrukiException(string message)
            : base(message)
        {
        }


        public OptimaWydrukiException(int _errorNumber, long TrnId, string message, Exception inner)
     : base(message + " dla dokumentu ID = " + TrnId, inner)
        {
            ErrorNumber = _errorNumber;
        }
       
        public OptimaWydrukiException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}


