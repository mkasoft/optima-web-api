﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using ADODB;
using CDNBase;
using CDNHeal;
using CDNHlmn;
using com.asoft.OptimaLib.dtoOptima;
using com.asoft.OptimaLib.dtoService;
using OptimaSDKLib.Base;

namespace com.asoft.Base
{
    public class DokumentyCommonTools

    {

        public static IDokumentHaMag ZnajdzDokumentPowiazanyPoOpisie(AdoSession sesja, ImportDokumentDto tra)
        {
            
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokumentOptimaPowiazany = null; 
            try
            {
                //Szukaj dokumentu                        
                dokumentOptimaPowiazany =
                    (IDokumentHaMag)dokumenty["TrN_NumerObcy='" + tra.Opis + "' and Trn_Anulowany=0"];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie paragonu." + tra.NumerObcy, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie paragonu." + tra.NumerObcy, e);
            }
            
            return dokumentOptimaPowiazany;
        }
        public static object GetSingleValue(CDNBase.AdoSession sesja, string query, bool configCnn)
        {
            List<decimal> IdDostaw = new List<decimal>();
            ADODB.Connection cn = configCnn ? sesja.ConfigConnection : sesja.Connection;
            //todo w dixi działa a to nie
            //ADODB.Recordset rs = new ADODB.RecordsetClass();

            ADODB.Recordset rs = new ADODB.Recordset();
            rs.Open(query, cn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);
            if (rs.RecordCount > 0)
            {
                return rs.Fields[0].Value;
            }
            else
                return null;
        }

        public static int GetIdFaktury(AdoSession sesja, string numerObcy)
        {
            if (String.IsNullOrEmpty(numerObcy)) return 0;
            int id = 0;
            id = Convert.ToInt32(GetSingleValue(sesja, String.Format("Select IsNull(TrN_TrNID, 0) From cdn.TraNag Where TrN_NumerObcy = '{0}' and TrN_Anulowany=0 ", numerObcy), false));
            return id;
        }
        public static IDokumentHaMag ZnajdzDokumentKorygowanyNieanulowany(AdoSession sesja, ImportDokumentDto tra)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokumentKorygowany = null;

            GetIdFaktury(sesja, tra.Opis);
            try
            {
                //Szukaj dokumentu
                dokumentKorygowany = (IDokumentHaMag)dokumenty["TrN_NumerObcy='" + tra.Opis + "' and Trn_Anulowany=0"];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie oryginału.", nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie oryginału." + tra.Opis,e);
            }
            if (dokumentKorygowany == null)
            {
                var exception = new InvalidOperationException("Błąd:  Nie został odszukany dokument korygowany" + tra.NumerPelny + "do dokumentu " +
                                                           tra.Opis);
                CommonToolsCommand.ZalogujBlad("Błąd:  Nie został odszukany dokument korygowany" + tra.NumerPelny + "do dokumentu " +
                                                           tra.Opis, nameof(DokumentyCommonTools), exception);
                throw exception;
            }
            return dokumentKorygowany;
        }
        public static IDokumentHaMag ZnajdzDokumentKorygowanyPoTrnId(AdoSession sesja, ImportDokumentDto tra)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokumentKorygowany = null;
            try
            {
                dokumentKorygowany =
                    (IDokumentHaMag)dokumenty["TrN_TrnId=" + GetIdFaktury(sesja, tra.Opis)];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie oryginału lub oryginał został anulowany" +
                                                           tra.Opis, nameof(DokumentyCommonTools), e);

                throw new InvalidOperationException("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie oryginału lub oryginał został anulowany" +
                                                           tra.Opis);
            }
            if (dokumentKorygowany == null)
            {
                var exception = new InvalidOperationException("Błąd: Brak dokumentu korygowanego. Dokument " + tra.NumerPelny + " Dokument korygowany " + tra.Opis);
                CommonToolsCommand.ZalogujBlad("Błąd: Brak dokumentu korygowanego. Dokument " + tra.NumerPelny + " Dokument korygowany " +
                                                           tra.Opis, nameof(DokumentyCommonTools), exception);
                throw exception;
            }
            return dokumentKorygowany;
        }

       
        public static void UstawDateDokumentu(IDokumentHaMag Dokument, ImportDokumentDto tra)
        {
           
   
            try
            {               
                if (tra.DataSprzedazy == null && tra.DataDok != null)
                    {
                        tra.DataSprzedazy = tra.DataDok;
                    }           
                if (tra.DataDok == null && tra.DataSprzedazy !=null)
                    {
                        tra.DataDok = tra.DataSprzedazy;
                    }

                Dokument.DataDok = GetDateOrDefaultToNow(tra.DataDok);
                Dokument.DataSprzedazy = GetDateOrDefaultToNow(tra.DataSprzedazy);
                Dokument.DataWys = GetDateOrDefaultToNow(tra.DataWys);

            }
            catch (Exception e)
            {
                // źle poszło weź bieżącą
                Dokument.DataDok = DateTime.Now;
                Dokument.DataSprzedazy = DateTime.Now;
                Dokument.DataWys = DateTime.Now;

                CommonToolsCommand.ZalogujBlad("Błąd - problem z ustawieniem daty: " + Dokument, nameof(DokumentyCommonTools), e);
              
            }
        }

        private static DateTime GetDateOrDefaultToNow(Nullable<System.DateTime> Date)
        {            
            if (Date != null)
            {
               return  (DateTime)Date;

            }
            return DateTime.Now;
        }

        public static IDokumentHaMag ZnajdzDokumentOptimaNieAnulowanyPoNumerzePelnym(AdoSession sesja, string nrDokumentu)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            //Szukaj dokumentu
            try
            {
                //Szukaj dokumentu
                var dokumentOptima = (IDokumentHaMag)dokumenty["TrN_NumerPelny='" + nrDokumentu + "' and Trn_Anulowany=0"];
                return dokumentOptima;
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie odszukano dokumentu nieanulowanego" + nrDokumentu, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Nie odszukano dokumentu nieanulowanego " + nrDokumentu);
            }
        }

        public static IDokumentHaMag ZnajdzDokumentPoTrNiD(AdoSession sesja, long TrnId)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            //Szukaj dokumentu
            try
            {
                //Szukaj dokumentu
                var dokumentOptima = (IDokumentHaMag)dokumenty["TrN_TrNID=" + TrnId ];
                return dokumentOptima;
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie odszukano dokumentu " + TrnId, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Nie odszukano dokumentu " + TrnId, e);
            }
        }

        public static void SprawdzCzyDokumentJuzIstnieje(AdoSession sesja, ImportDokumentDto tra, IDokumentHaMag dokument)
        {
            if (dokument.NumerNr == 0)
            {
                //Numer dokumentu z autonumeracji Optimy - kontynuuję dodawanie dokumentu bez sprawdzania czy istnieje dodawany dokument.
                return;
            }

            try
            {
                ZnajdzDokumentOptimaNieAnulowanyPoNumerzePelnym(sesja, dokument.NumerPelny);
                throw new DataException("Błąd: Dokument " + dokument.NumerPelny + " już istnieje w Optimie. ERR-10001");
            }
            catch (InvalidOperationException e)
            {
                CommonToolsCommand.ZalogujBlad("Sprawdzono: " + dokument.NumerPelny +
                                  " nie został znaleziony w Optima - kontynuuję dodawanie dokumentu.", nameof(DokumentyCommonTools), e);
            }
        }

        public static Magazyn UstawMagazyn(AdoSession sesja, ImportDokumentDto tra)
        {

            try
            {
                var rMagazyn = sesja.CreateObject("CDN.Magazyny").Item("Mag_Symbol='" + tra.MagazynZrodlowy + "'");

                return rMagazyn;
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie jest zdefiniowany w optimie magazyn:" +
                                                          tra.MagazynZrodlowy, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Błąd - Nie jest zdefiniowany w optimie magazyn:" + tra.MagazynZrodlowy);
            }
        }

        public static Magazyn UstawMagazynDocelowy(AdoSession sesja, ImportDokumentDto tra)
        {
            try
            {
                var rMagazyn = sesja.CreateObject("CDN.Magazyny").Item("Mag_Symbol='" + tra.MagazynDocelowy + "'");

                return rMagazyn;
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie jest zdefiniowany w optimie magazyn:" +
                                                           tra.MagazynDocelowy, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Błąd - Nie jest zdefiniowany w optimie magazyn:" +
                                                           tra.MagazynDocelowy, e);
            }
        }

        public static IDefinicjaDokumentu ZnajdzDefninicjeDokumentuOptimy(AdoSession sesja, ImportDokumentDto tra)
        {
            DefinicjeDokumentow definicjeDokumentow =
                (DefinicjeDokumentow)sesja.CreateObject("CDN.DefinicjeDokumentow", null);

            IDefinicjaDokumentu definicjaDokumentu = null;
            try
            {
                definicjaDokumentu = (DefinicjaDokumentu)definicjeDokumentow["Ddf_Symbol='" + tra.ExportKod + "'"];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie jest zdefiniowany w optimie dokument:" + "-" +
                                                           tra.NumerPelny + " kod=" + tra.ExportKod, nameof(DokumentyCommonTools), e);

                throw new InvalidOperationException("Błąd - Nie jest zdefiniowany w optimie dokument:" + "-" +
                                                           tra.NumerPelny + " kod=" +tra.ExportKod, e);
            }
            return definicjaDokumentu;
        }

       

        public static IDokumentHaMag ZnajdzDokumentOptimyPoNumerPelny(AdoSession sesja, ImportDokumentDto tra)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokument = null;

            try
            {
                //Szukaj dokumentu
                dokument = (IDokumentHaMag)dokumenty["TrN_NumerPelny='" + tra.NumerObcy + "'"];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - Nie jest zdefiniowany w optimie dokument: " + tra.NumerPelny + " Opis: " + tra.Opis, nameof(DokumentyCommonTools), e);
                throw new InvalidOperationException("Błąd - Nie jest zdefiniowany w optimie dokument: " + tra.NumerPelny + " Opis: " + tra.Opis + tra.Opis, e);
            }
            return dokument;
        }
        public static void OkreslKategorieDokumentu(AdoSession sesja, ImportDokumentDto tra, IDokumentHaMag dokument)
        {
            if (!String.IsNullOrEmpty(tra.Kategoria))
            {
                try
                {
                    CDNBase.ICollection Kategorie = (CDNBase.ICollection)(sesja.CreateObject("CDN.Kategorie", null));
                    CDNHeal.Kategoria KategoriaDokumentu =
                        (CDNHeal.Kategoria)Kategorie["Kat_KodSzczegol = '" + tra.Kategoria + "'"];
                    dokument.Kategoria = KategoriaDokumentu;
                }
                catch (Exception e)
                {
                    CommonToolsCommand.ZalogujBlad("Nie udało się określić kategorii dokumentu: " + dokument.NumerPelny, nameof(DokumentyCommonTools), e);
                }
            }
        }
        
        public static decimal WyodrebnijNrDokumentu(ImportDokumentDto tra)
        {
            Decimal nr_dokumentu = 0M;
            if (tra.CzyPos == 1)
            {
                string[] Split = tra.NumerPelny.Split(new Char[] { '/' });
                Decimal.TryParse(Split[1], out nr_dokumentu);
            }
            else
            {
                if (tra.NrDok != null)
                {
                    nr_dokumentu = (decimal)tra.NrDok;
                }
            }
            return nr_dokumentu;
        }

        public static void ZamienRodzajDokumentuDlaKorekty(ImportDokumentDto tra, IDokumentHaMag dokument)
        {
            if (tra.CzyKorekta == 1)
            {
                if (dokument.TypDokumentu == (int)OptimaTypDokumentu.FakturaSprzedazy) //302
                {
                    // FAKI 302001; 
                    // FAKW 302002; 
                    // FAKV 302003;

                    dokument.Rodzaj = 302007;
                }
                else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.Paragon)//305
                {
                    dokument.Rodzaj = 305001;
                }
                else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.WydanieZewnetrzne) //306
                {
                    dokument.Rodzaj = 306001;
                }
                else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.RozchodWewnetrzny) //304
                {
                    dokument.Rodzaj = 304001;
                }
                else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzyjecieWewnetrzne) //303
                {
                    dokument.Rodzaj = 303001;
                }
                else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzyjecieZewnetrzne) //307
                {
                    /*  Faktura korygująca
                             Określa, czy faktura jest fakturą korygującą, oraz typ korekty: 
                             0 - faktura zwykła 
                             1 - korekta ilości 
                             2 - korekta wartości (ceny) 
                             3 - korekta Vat 
                             4 - nota korygująca 
                             5 - korekta graniczna 
                             6 - korekta graniczna UE
                             */
                    //Rodzaj korekty
                    if ("1".Equals(tra.CzyKorektaCeny))
                    {
                        dokument.Rodzaj = 307002;
                    }
                    else
                    {
                        dokument.Rodzaj = 307001;
                    }
                }
            }
        }
        
        public static IDokumentHaMag ZnajdzDokumentKorygowany(AdoSession sesja, ImportDokumentDto tra)
        {
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokumentKorygowany = null;

            var id = DokumentyCommonTools.GetIdFaktury(sesja, tra.Opis);
            try
            {
                //Szukaj dokumentu
                dokumentKorygowany = (IDokumentHaMag)dokumenty["TrN_NumerObcy='" + tra.Opis + "'"];
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Błąd: Dokument " + tra.NumerPelny +
                                                           " nie posiada w Optimie oryginału." + tra.Opis);
            }
            if (dokumentKorygowany == null)
            {
                 throw new InvalidOperationException("Błąd: Dokument " + tra.NumerPelny + "Dokument korygowany " + tra.NumerObcy + Environment.NewLine);
            }
            return dokumentKorygowany;
        }



        public static void SetProperty(object o, string name, object value)
        {
            if (o == null)
                return;
            o.GetType().InvokeMember(name, BindingFlags.SetProperty, null, o, new object[] { value });
        }

        public static int BlokadaPlatnosci_Dokument(AdoSession sesja, decimal IDDokumentu)
        {
            int Id = 0;
            Id = Convert.ToInt32(UpdateSingleValueDokument(sesja, String.Format("Select TrN_TrNID,TrN_BlokadaPlatnosci From cdn.TraNag Where TrN_TrNID = '{0}'", IDDokumentu), false));
            return Id;
        }
        public static object UpdateSingleValueDokument(AdoSession sesja, string query, bool configCnn)
        {
            Connection cn = configCnn ? sesja.ConfigConnection : sesja.Connection;
            //todo w dixi działa a to nie
            //ADODB.Recordset rs = new ADODB.RecordsetClass();

            Recordset rs = new Recordset();
            try
            {
                rs.Open(query, cn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic, 1);
                if (rs.RecordCount > 0)
                {
                    rs.Update("TrN_BlokadaPlatnosci", 1);
                    int Id = (int)rs.Fields[0].Value;
                    return Id;
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                //  cn = null;
            }
            return null;
        }

       


    }
}