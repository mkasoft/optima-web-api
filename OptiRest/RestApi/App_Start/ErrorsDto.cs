﻿using com.asoft.OptimaLib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.App_Start
{
    public class ErrorsDto
    {
        
        private Exception exception;

        public int ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
       
        public List<string> ErrorsStack { get; set; }
        public string ErrorPayload { get; set; }

        public ErrorsDto(string errorMessage, string errorPayload)
        {
            ErrorsStack = new List<string>();            
            ErrorMessage = errorMessage;
            ErrorPayload = errorPayload;
        }

        public ErrorsDto(OptimaDokumentException optimaAddDokumentException,  string errorMessage, string errorPayload)
        {
            ErrorsStack = new List<string>();
            ErrorNumber = optimaAddDokumentException.ErrorNumber;
            ErrorMessage = errorMessage;
            ErrorPayload = errorPayload;
        }

        public ErrorsDto(OptimaKontrahentException optimaKontrahentException, string errorMessage, string errorPayload)
        {
            ErrorsStack = new List<string>();
            ErrorNumber = optimaKontrahentException.ErrorNumber;
            ErrorMessage = errorMessage;
            ErrorPayload = errorPayload;
        }

        public ErrorsDto(OptimaWydrukiException optimaWydrukiException, string errorMessage, string errorPayload)
        {
            ErrorsStack = new List<string>();
            ErrorNumber = optimaWydrukiException.ErrorNumber;
            ErrorMessage = errorMessage;
        }

        public ErrorsDto(OptimaTowaryException optimaTowaryException, string errorMessage, string errorPayload)
        {
            ErrorsStack = new List<string>();
            ErrorNumber = optimaTowaryException.ErrorNumber;
            ErrorMessage = errorMessage;
            ErrorPayload = errorPayload;
        }
    }
}