﻿using com.asoft.OptimaLib.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.asoft.OptimaLib.Base;
using OptimaSDKLib.Base;


namespace OptimaLibUnitTest.daoService
{

    [TestClass]
    public class KontrahenciTest
    {
        public KontrahenciTest()
        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();
        }

        [ClassCleanup()]
        public static void CleanupTestSuite()
        {
            //to do cleanUp
        }


        [TestMethod]
        public void PobierzKontrahentowPoDacie()
        {
            IKontrahenciCommand kontrahenciCommand = new KontrahenciCommand();          
            var sesja = OptimaSessionPool.GetSession();   
            var kontrahenciLista = kontrahenciCommand.ListaKontrahentow(sesja, "2017-13-02");
            OptimaSessionPool.PutSession(sesja);
        }
        [TestMethod]
        public void PobierzKontrahentowWszystkichGdyParametrPusty()
        {

            IKontrahenciCommand kontrahenciCommand = new KontrahenciCommand();
            var sesja = OptimaSessionPool.GetSession();
            var kontrahenciLista = kontrahenciCommand.ListaKontrahentow(sesja);
            OptimaSessionPool.PutSession(sesja);
        }

       
    }
}