﻿using com.asoft.OptimaLib.dtoOptima;
using System;

namespace com.asoft.OptimaLib.dtoCriteria
{
    public class DokumentyKryteriaDto
    {
        public string NrDokumentu { get; set; }
        public string TypDokumentu { get; set; }
        public string RodzajDokumentu { get; set; }
        public DateTime DataPoczatek { get; set; }
        public DateTime DataKoniec { get; set; }

        public string ToExceptionString()
        {
            return "Kryteria: NrDokumentu =" + NrDokumentu + " TypDokumentu = " + TypDokumentu + "RodzajDokumentu" + RodzajDokumentu + " DataPoczatek =" + DataPoczatek + "DataKoniec=" + DataKoniec + Environment.NewLine;
        }
    }
    
}
