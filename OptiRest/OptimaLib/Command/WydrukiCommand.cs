﻿using System;
using System.Configuration;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.tools;

namespace OptimaSDKLib.Base
{

    public class WydrukiCommand : IWydrukiCommand
    {
        private const int ID_WYDRUKU_STANDARDOWEGO_FAKTURY = 1258;
        private const int FORMAT_PLIKU_DOCELOWEGO_PDF = 2;
        private string SCIEZKA = ConfigurationSettings.AppSettings["SCIEZKA"];
        private string FILENAME_PREFIX = ConfigurationSettings.AppSettings["FILENAME_PREFIX"];

        public WydrukiCommand()
        {

        }

        public string Pdf(AdoSession sesja, long TrNID)
        {            
          
            Type type = Type.GetTypeFromProgID("CDN.WydrFormat", true);
            dynamic Format = Activator.CreateInstance(type);
            Format.ZrodloID = 0;
            Format.ID = ID_WYDRUKU_STANDARDOWEGO_FAKTURY;
            //Id wydruku uzyskane poprzez eskport listy wydruków 
            //STD_1257_Faktura (bez stopek pośrednich).rpt
            //STD_1258_Wzór standard.rpt
            //STD_1259_Duplikat.rpt

            Format.FiltrSQL = "TrN_TrNID = " + TrNID;
            Format.Urzadzenie = WydrukUrzadzenie.Plik;
            Format.FormatPlikuDocelowego = FORMAT_PLIKU_DOCELOWEGO_PDF;
            Format.PlikDocelowy = SCIEZKA+FILENAME_PREFIX + TrNID + "_" + System.DateTime.Now.Millisecond + ".pdf";
            CDNLib.Dictionary ZmiennaDyn = (CDNLib.Dictionary)Activator.CreateInstance(Type.GetTypeFromProgID("CDNLib.Dictionary"));
            Format.Wykonaj(ZmiennaDyn);

            return Format.PlikDocelowy;
        }
    }
}
 