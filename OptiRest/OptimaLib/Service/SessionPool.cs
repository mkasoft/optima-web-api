﻿using System;
using System.Collections.Concurrent;

namespace com.asoft.OptimaLib.Service
{
    public class SessionPool<T>
    {
        private ConcurrentBag<T> _objects;
        private Func<T> _objectGenerator;

        public SessionPool(Func<T> objectGenerator,
            int boundedCapacity,
            int initializeCount = 0)
        {
            if (objectGenerator == null) throw new ArgumentNullException("objectGenerator");
            _objects = new ConcurrentBag<T>(new BlockingCollection<T>(new ConcurrentBag<T>(), boundedCapacity));
            _objectGenerator = objectGenerator;
            if (initializeCount > 0)
            {
                int counter = 0;
                while (counter++ < initializeCount)
                {
                    _objects.Add(_objectGenerator());
                }
            }
        }

        public T GetObject()
        {
            T item;
            if (_objects.TryTake(out item)) return item;
            return _objectGenerator();
        }

        public void PutObject(T item)
        {
            _objects.Add(item);
        }
    }
}
