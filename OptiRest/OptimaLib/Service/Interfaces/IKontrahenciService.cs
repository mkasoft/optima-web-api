﻿using System.Collections.Generic;
using CDNBase;
using com.asoft.OptimaLib.dtoService;

namespace com.asoft.OptimaLib.Service
{
    public interface IKontrahenciService
    {

        List<KontrahentDto> ListaKontrahentow(string sql);
        List<KontrahentDto> ListaKontrahentow();
        
    }
}