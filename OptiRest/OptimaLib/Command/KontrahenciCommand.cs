﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ADODB;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;

namespace OptimaSDKLib.Base
{
    public class KontrahenciCommand : IKontrahenciCommand
    {
        public KontrahenciCommand()
        {

        }
        public List<KontrahentDto> ListaKontrahentow(AdoSession sesja)
        {
           
            string sql = "";
            try
            {
               
                sql = SqlKontrahenciWszyscy();
                return PobierzKontrahentow(sesja, sql); 

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd czytania kontrahentów z Optimy) - " + e.Message, nameof(KontrahenciCommand), e);
                throw new OptimaKontrahentException(20001,sql, "ERROR -  (Błąd czytania kontrahentów z Optimy)- ", e);
            }          
        }
    

            public List<KontrahentDto> ListaKontrahentow(AdoSession sesja, String dataOstatniejModyfikacji)
        {
           
            string sql = "";
            try
            {
                
             
                if (String.IsNullOrEmpty(dataOstatniejModyfikacji))
                {
                    sql = SqlKontrahenciWszyscy();
                }
                else
                {
                    sql = SqlKontrahenciZmodyfikowaniPoPodanejDacie(dataOstatniejModyfikacji);
                }

                return PobierzKontrahentow(sesja, sql); 

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -   (Błąd czytania kontrahentów z Optimy z paramatrem data ostatniej aktualizacji)- " + e.Message, nameof(KontrahenciCommand), e);
                throw new OptimaKontrahentException(20002,sql, "ERROR -  (Błąd czytania kontrahentów z Optimy z paramatrem data ostatniej aktualizacji)- ", e);
            }
                   
        }


        //** Dostarcza listę kontrahentów z Optimy zmodyfikowaną po podanej dacie


        private List<KontrahentDto> PobierzKontrahentow(AdoSession sesja, String sql)
        {
            List<KontrahentDto> listaKontrahenow = new List<KontrahentDto>();
            var cn = sesja.Connection;
            var rs = new Recordset();

            rs.Open(sql, cn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly, 1);
            while (!rs.EOF)
            {
                var kontrahent = new KontrahentDto();
                kontrahent.IdKthCnt = Convert.ToInt64(rs.Fields["KNT_GIDNUMER"].Value);
                kontrahent.Kod = (string)rs.Fields["KNT_AKRONIM"].Value;
                kontrahent.Nazwa1 = (string)rs.Fields["KNT_NAZWA1"].Value;
                kontrahent.Nazwa2 = (string)rs.Fields["KNT_NAZWA2"].Value;
                kontrahent.Nazwa3 = (string)rs.Fields["KNT_NAZWA1"].Value;
                kontrahent.KodPocztowy = (string)rs.Fields["KNT_KODP"].Value;
                kontrahent.Miejscowosc = (string)rs.Fields["KNT_MIASTO"].Value;
                kontrahent.Ulica = (string)rs.Fields["KNT_ULICA"].Value;
                kontrahent.NrDomu = (string)rs.Fields["Knt_NrDomu"].Value;
                kontrahent.NrLokalu = (string)rs.Fields["Knt_NrLokalu"].Value;

                kontrahent.Dodatkowe = (string)rs.Fields["KNT_ADRES"].Value;
                kontrahent.Nip = (string)rs.Fields["KNT_NIPE"].Value;
                kontrahent.Kraj = (string)rs.Fields["KNT_KRAJ"].Value;
                kontrahent.TimeStampModyfikacji = (DateTime)rs.Fields["KNT_TS_MOD"].Value;
                kontrahent.Regon = (string)rs.Fields["KNT_REGON"].Value;
                kontrahent.CzyWstrzTrans = Convert.ToBoolean(rs.Fields["KNT_NIEAKTYWNY"].Value);

                kontrahent.CzyOdbiorca = Convert.ToBoolean(rs.Fields["KNT_RODZAJ_ODBIORCA"].Value);
                kontrahent.CzyDostawca = Convert.ToBoolean(rs.Fields["KNT_RODZAJ_DOSTAWCA"].Value);
                kontrahent.Telefon = (string)rs.Fields["Knt_Telefon1"].Value;
                kontrahent.Fax = (string)rs.Fields["KNT_FAX"].Value;
                kontrahent.Wojewodztwo = (string)rs.Fields["Knt_Wojewodztwo"].Value;
                kontrahent.Email = (string)rs.Fields["Knt_Email"].Value;
                kontrahent.Www = (string)rs.Fields["Knt_URL"].Value;

                kontrahent.FormaPlatId = Convert.ToInt64(rs.Fields["Knt_FplID"].Value);
                kontrahent.PlatTermin = Convert.ToInt64(rs.Fields["Knt_Termin"].Value);

                rs.MoveNext();

                listaKontrahenow.Add(kontrahent);
            }


            return listaKontrahenow;


        }

        private static string UtworzSql(string dataOstatniejModyfikacji)
        {
            String SQL;
            if (String.IsNullOrEmpty(dataOstatniejModyfikacji))
            {
                SQL = SqlKontrahenciWszyscy();
            }
            else
            {
                SQL = SqlKontrahenciZmodyfikowaniPoPodanejDacie(dataOstatniejModyfikacji);
            }
            return SQL;
        }

        private static string SqlKontrahenciZmodyfikowaniPoPodanejDacie(string dataOstatniejModyfikacji)
        {

            SqlParameter paramDataOstatniejModyfikacji = new SqlParameter("@Param1", SqlDbType.Int, 40);
            paramDataOstatniejModyfikacji.Value = dataOstatniejModyfikacji;
           
            //Todo        dataOstatniejModyfikacji - sprawdzić format przekazywanej daty w testach wychodzi rrrr-dd-mm
            String SQL = "select KNT_GIDNUMER,KNT_KOD KNT_AKRONIM,KNT_NAZWA1,KNT_NAZWA2,KNT_NAZWA3,KNT_KODPOCZTOWY KNT_KODP,KNT_MIASTO,KNT_ULICA,"
                         + " Knt_NrDomu ,Knt_NrLokalu,"
                         + " KNT_ADRES2 KNT_ADRES,KNT_NIPE,KNT_KRAJ,KNT_TS_MOD,KNT_REGON,KNT_NIEAKTYWNY,"
                         +
                         " KNT_RODZAJ_ODBIORCA,KNT_RODZAJ_DOSTAWCA,Knt_Telefon1, KNT_FAX, Knt_Wojewodztwo,Knt_Email,Knt_URL, Knt_FplID, Knt_Termin"
                         
                        
                         + " from cdn.Kontrahenci k"
                         + " where KnT_TS_Mod > '" + dataOstatniejModyfikacji + "'"
                         + " order by KNT_GIDNUMER";
            return SQL;
        }

        //Todo - nie udała sie jeszcze procedura - nie podmienia
        private static string  UzupelnijSqlParametrami(SqlParameter paramDataOstatniejModyfikacji)
        {      
            var sqlCommand = new SqlCommand(
                "select KNT_GIDNUMER,KNT_KOD KNT_AKRONIM,KNT_NAZWA1,KNT_NAZWA2,KNT_NAZWA3,KNT_KODPOCZTOWY KNT_KODP,KNT_MIASTO,KNT_ULICA,"
                + " Knt_NrDomu ,Knt_NrLokalu,"
                + " KNT_ADRES2 KNT_ADRES,KNT_NIPE,KNT_KRAJ,KNT_TS_MOD,KNT_REGON,KNT_NIEAKTYWNY,"
                +
                " KNT_RODZAJ_ODBIORCA,KNT_RODZAJ_DOSTAWCA,Knt_Telefon1, KNT_FAX, Knt_Wojewodztwo,Knt_Email,Knt_URL, Knt_FplID, Knt_Termin,"
                + " (SELECT max(atr.KnA_WartoscTxt) FROM CDN.KntAtrybuty atr"
                +
                " where  atr.Kna_PodmiotId = k.Knt_KntId and KnA_DeAId = (SELECT	max(DeA_DeAId) FROM [CDN].[DefAtrybuty] WHERE [DeA_Kod] = 'WSKOPAK')) as WSKOPAK"
                + " from cdn.Kontrahenci k"
                + " where KnT_TS_Mod > @Param1"
                + " order by KNT_GIDNUMER", null);
            sqlCommand.Parameters.Add(paramDataOstatniejModyfikacji);
           return  sqlCommand.CommandText;
        }

        private static string SqlKontrahenciWszyscy()
        {
            String SQL = "select KNT_GIDNUMER,KNT_KOD KNT_AKRONIM,KNT_NAZWA1,KNT_NAZWA2,KNT_NAZWA3,KNT_KODPOCZTOWY KNT_KODP,KNT_MIASTO,KNT_ULICA,"
                         + " Knt_NrDomu ,Knt_NrLokalu,"
                         + " KNT_ADRES2 KNT_ADRES,KNT_NIPE,KNT_KRAJ,KNT_TS_MOD,KNT_REGON,KNT_NIEAKTYWNY,"
                         +
                         " KNT_RODZAJ_ODBIORCA,KNT_RODZAJ_DOSTAWCA,Knt_Telefon1, KNT_FAX, Knt_Wojewodztwo,Knt_Email,Knt_URL, Knt_FplID, Knt_Termin"
                       + " from cdn.Kontrahenci k" 
                         + " order by KNT_GIDNUMER";
            return SQL;
        }

      
    }
}