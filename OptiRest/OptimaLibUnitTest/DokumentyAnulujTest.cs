﻿using System;
using System.Collections.Generic;
using com.asoft.OptimaLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.asoft.OptimaLib.dtoOptima;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Base;

namespace OptimaLibUnitTest
{
    [TestClass]    

        public class DokumentyAnulujTest
    {

        public DokumentyAnulujTest()
        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();
        }

        [TestMethod]

        public void AnulujDokument_PoNumerzePelnym()
        {

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

            var tra = ToolsForTest.UtworzTransakcje(ImportowanyTypDokumentu.FakturaSprzedazy, "126/FS/2017");
            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR", 2);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR", 2);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR", 2);
            tra.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();

            tra.PozycjeDokumentuImportowanego.Add(pozycja1);
            tra.PozycjeDokumentuImportowanego.Add(pozycja2);
            tra.PozycjeDokumentuImportowanego.Add(pozycja3);

            tra.ExportKod = ImportowanyTypDokumentu.FakturaSprzedazy;
            tra.CzyZapisBufor = 0;

            var dokumentFs = new DokumentCommand().FaktureSprzedazy_Dodaj(tra, sesja);
            DokumentyCommandAnuluj.AnulujDokumentOptimaPoNumerzePelnym(sesja, dokumentFs.NumerPelny);
        }
        [TestMethod]
        public void AnulujDokument_Zatwierdzony()
        {
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

            var tra = ToolsForTest.UtworzTransakcje(ImportowanyTypDokumentu.FakturaSprzedazy, "126/FS/2017");
            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR", 2);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR", 2);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR", 2);
            tra.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();

            tra.PozycjeDokumentuImportowanego.Add(pozycja1);
            tra.PozycjeDokumentuImportowanego.Add(pozycja2);
            tra.PozycjeDokumentuImportowanego.Add(pozycja3);

            tra.ExportKod = ImportowanyTypDokumentu.FakturaSprzedazy;
            tra.CzyZapisBufor = 0;

            var dokumentFs = new DokumentCommand().FaktureSprzedazy_Dodaj(tra, sesja);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentFs);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Błąd: Dokument jest w buforze nie można go anulować")]
        public void AnulujDokument_Bufor()
        {
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

            var tra = ToolsForTest.UtworzTransakcje(ImportowanyTypDokumentu.FakturaSprzedazy, "126/FS/2017");
            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR", 2);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR", 2);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR", 2);
            tra.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();

            tra.PozycjeDokumentuImportowanego.Add(pozycja1);
            tra.PozycjeDokumentuImportowanego.Add(pozycja2);
            tra.PozycjeDokumentuImportowanego.Add(pozycja3);

            tra.ExportKod = ImportowanyTypDokumentu.FakturaSprzedazy;
            tra.CzyZapisBufor = 0;

            var dokumentFs = new DokumentCommand().FaktureSprzedazy_Dodaj(tra, sesja);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentFs);
            
        }
    }
}