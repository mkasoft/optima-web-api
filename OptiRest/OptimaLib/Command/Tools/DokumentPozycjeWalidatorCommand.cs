﻿using com.asoft.OptimaLib.dtoService;


namespace OptimaSDKLib.Base
{
    public class DokumentPozycjeWalidatorCommand
    {
        public static void SprawdzPozycjeDokumentuZrodlowegoFz(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);

            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }

            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto transakcji");
            }

        }

        public static void SprawdzPozycjeDokumentuZrodlowegoFs(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);
            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }

            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto transakcji");
            }

        }
        public static void SprawdzPozycjeDokumentuZrodlowegoPz(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);

            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }

        }
        public static void SprawdzPozycjeDokumentuZrodlowegoRwOrPw(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);
            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }
            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto transakcji");
            }
            if (tpz.Netto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona wartość netto ");
            }
            if (tpz.KosztWlasny == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślony koszt własny ");
            }
            if (tpz.Brutto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona wartość brutto");
            }
            if (tpz.CenaBrutto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena sprzedaży ");
            }
            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto ");
            }           

        }



        public static void SprawdzPozycjeDokumentuZrodlowegoWz(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);
            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }
            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto transakcji");
            }
            if (tpz.Netto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona wartość netto ");
            }
            if (tpz.KosztWlasny == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślony koszt własny ");
            }
            if (tpz.Brutto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona wartość brutto");
            }
            if (tpz.CenaBrutto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena sprzedaży ");
            }
            if (tpz.CenaNetto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena netto ");
            }
        }
        public static void SprawdzPozycjeDokumentuZrodlowegoWkaPka(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            WspolneWarunkiNaPozycje(tra, tpz);

            if (tpz.Stawka == null || tpz.Flaga == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona stawka VAT ");
            }
            if (tpz.Ilosc == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona ilość ");
            }

            if (tpz.CenaBrutto == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślona cena brutto");
            }
        }

        private static void WspolneWarunkiNaPozycje(ImportDokumentDto tra, ImportDokumentPozycjaDto tpz)
        {
            if (tpz.TowarKod == null)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " +
                                                           tpz.TowarKod + " nieokreślony kod towaru ");
            }
        }

    }
}