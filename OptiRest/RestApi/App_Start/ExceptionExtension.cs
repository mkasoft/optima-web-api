﻿using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.App_Start
{
    public static class ExceptionExtension
    {
        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem,
            Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static Exception GetOriginalException(this Exception ex)
        {
            if (ex.InnerException == null) return ex;

            return ex.InnerException.GetOriginalException();
        }

        public static string GetaAllMessages(this Exception exception, string ErrorMessage )
        {
            var messages = exception.FromHierarchy(ex => ex.InnerException)
                .Select(ex => ex.Message);
            ErrorsDto errDto;
            if ("OptimaDokumentException".Equals(exception.GetType().Name))
            {

                errDto = new ErrorsDto((OptimaDokumentException)exception, ErrorMessage, ((OptimaDokumentException)exception).GetPayloadError());
            }
            else if ("OptimaKontrahentException".Equals(exception.GetType().Name))
            {
                errDto = new ErrorsDto((OptimaKontrahentException)exception, ErrorMessage, ((OptimaKontrahentException)exception).GetPayloadError());
            }
            else if ("OptimaTowaryException".Equals(exception.GetType().Name))
            {
                errDto = new ErrorsDto((OptimaTowaryException)exception, ErrorMessage, ((OptimaTowaryException)exception).GetPayloadError());
            }
            else if ("OptimaWydrukiException".Equals(exception.GetType().Name))
            {
                errDto = new ErrorsDto((OptimaWydrukiException)exception, ErrorMessage, "");
            }
            else
            {
                errDto = new ErrorsDto(ErrorMessage, "");
            }

            foreach (var m in messages)
            {
                errDto.ErrorsStack.Add(m);
            }

            
            return JsonConvert.SerializeObject(errDto, Formatting.None);
        }
    }
}