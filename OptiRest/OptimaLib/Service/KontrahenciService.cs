﻿using System;
using System.Collections.Generic;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoService;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib.Service
{
    public class KontrahenciService : IKontrahenciService
    {

        private readonly IKontrahenciCommand _kontrahenciCommand;


        public KontrahenciService(IKontrahenciCommand kontrahenciCommand)
        {
            _kontrahenciCommand = kontrahenciCommand ?? throw new ArgumentNullException(nameof(kontrahenciCommand));
        }
        public List<KontrahentDto> ListaKontrahentow()
        {
            AdoSession sesja = null;
            string sql = "";
            try
            {
                sesja = OptimaSessionPool.GetSession();
                return _kontrahenciCommand.ListaKontrahentow(sesja);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd czytania kontrahentów z Optimy) - " + e.Message,  nameof(KontrahenciService),e);
                throw new OptimaKontrahentException(20003, sql, "ERROR -  (Błąd czytania kontrahentów z Optimy)- błąd serwisu.", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
    

            public List<KontrahentDto> ListaKontrahentow(String dataOstatniejModyfikacji)
        {
            AdoSession sesja = null;
            string sql = "";
            try
            {
                sesja = OptimaSessionPool.GetSession();
                return _kontrahenciCommand.ListaKontrahentow(sesja, dataOstatniejModyfikacji);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd czytania kontrahentów z Optimy) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaKontrahentException(20004,sql, "ERROR -  (Błąd czytania kontrahentów z Optimy)- z datą modyfikacji - błąd serwisu.", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }          
        }





       

      
    }
}