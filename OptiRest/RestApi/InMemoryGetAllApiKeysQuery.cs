﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RestApi
{
    public class InMemoryGetAllApiKeysQuery : IGetAllApiKeysQuery
    {
        private  ApiKeyList apiKeyList;

        public Task<IReadOnlyDictionary<string, ApiKey>> ExecuteAsync()
        {
            try
            {
                if (apiKeyList == null)
                {
                    string path = HttpContext.Current.Server.MapPath("~/App_Data/ApiKeysDb.xml");

                    System.Xml.Serialization.XmlSerializer reader =
                        new System.Xml.Serialization.XmlSerializer(typeof(ApiKeyList));

                    System.IO.StreamReader file = new System.IO.StreamReader(path);
                    apiKeyList = (ApiKeyList) reader.Deserialize(file);
                    file.Close();
                }

                IReadOnlyDictionary<string, ApiKey> readonlyDictionary = apiKeyList.ApiKeys.ToDictionary(x => x.Key, x => x);
                return Task.FromResult(readonlyDictionary);
            } catch (Exception e)
            {
                throw new Exception("Blad odczytu pliku api keys !");
            }
            

            
        }
    }
}