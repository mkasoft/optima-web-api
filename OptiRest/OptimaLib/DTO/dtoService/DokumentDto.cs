﻿using System;
using System.Collections.Generic;

namespace com.asoft.OptimaLib.dtoService
{
    public class DokumentDto
    {
        public override string ToString()
        {
            return "Dokument" + " TRN_TRNID=" + Id + " TrN_Rodzaj= " + Rodzaj + " " + NumerPelny + " netto=" + Netto + " vat" + Vat
                 + " brutto=" + Brutto + " MAGAZYN= " + MagazynZrodlowy + " Knt_Kod=" + PodmiotAkronim + " Anulowany = " + Anulowany
                 + " DateTime=" + DataWyst;
        }
        public long Id { get; set; }

        public int Rodzaj { get; set; }

        public string NumerPelny { get; set; }

        public decimal Netto { get; set; }

        public decimal Vat { get; set; }

        public decimal Brutto { get; set; }

        public string MagazynZrodlowy { get; set; }


        public string MagazynDocelowy { get; set; }

        public string PodmiotAkronim { get; set; }

        public int Anulowany { get; set; }

        public DateTime DataWyst { get; set; }

        public DateTime DataDok { get; set; }

        public List<DokumentPozycjaDto> Pozycje { get; set; }

        public string NumerPelnyKorekty { get; set; }
    }
}