﻿using System;
using ADODB;
using CDNBase;
using com.asoft.OptimaLib.Base;

using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib.Service
{
    public class WydrukiService : IWydrukiService
    {
        private readonly IWydrukiCommand _wydrukiCommand;
        public WydrukiService(IWydrukiCommand wydrukiCommand)
        {
            _wydrukiCommand = wydrukiCommand ?? throw new ArgumentNullException(nameof(wydrukiCommand));
        }

        public string pdf(long TrnId)
        {

            AdoSession sesja = null;
           
            try
            {
                sesja = OptimaSessionPool.GetSession();

                return _wydrukiCommand.Pdf(sesja, TrnId);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd generowania wydruku pdf) - " + e.Message, nameof(WydrukiService), e);
                throw new OptimaWydrukiException(40001,TrnId, "ERROR -  (Błąd generowania wydruku pdf)", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        
    }
}