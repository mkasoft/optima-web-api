﻿using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class MmController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;
        public MmController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }
        [HttpPost]
        public DokumentDto AddMm([FromBody]ImportDokumentDto impTranDto)
        {
            return _dokumentyService.DodajDokumentMm(impTranDto);
        }
    }
}