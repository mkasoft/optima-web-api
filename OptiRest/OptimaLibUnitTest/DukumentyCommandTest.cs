﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaSDKLib.Base;
using OptimaLibUnitTest;
using System.Collections.Generic;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib;
using com.asoft.OptimaLib.dtoOptima;
using CDNHlmn;
using com.asoft.OptimaLib.Base;
using com.asoft.Base;

namespace OptimaSDKLib.Test
{
    [TestClass]
    public class OptimaCommandTest
    {
        public OptimaCommandTest()
        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();
        }

        /*
          //[TestInitialize] and [TestCleanUp]
          [ClassInitialize()]
          public static void InitTestSuite(TestContext testContext)
          {

          }

          [ClassCleanup()]
          public static void CleanupTestSuite()
          {
              //
          }

          [TestInitialize()]
          public static void InitTest(TestContext testContext)
          {
             //
          }

          [TestCleanup()]
          public static void TestClean(TestContext testContext)
          {
             //
          }

      */
        [TestMethod]
        public void FakturaSprzeadzyOrazParagon()
        {
                var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

                var tra = ToolsForTest.UtworzTransakcje(ImportowanyTypDokumentu.FakturaSprzedazy, "126/FS/2017");
                var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR", 2);
                var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR", 2);
                var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR", 2);
                tra.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();

                tra.PozycjeDokumentuImportowanego.Add(pozycja1);
                tra.PozycjeDokumentuImportowanego.Add(pozycja2);
                tra.PozycjeDokumentuImportowanego.Add(pozycja3);

                tra.ExportKod = ImportowanyTypDokumentu.FakturaSprzedazy;
                tra.CzyZapisBufor = 0;
                var dokumentFs = new DokumentCommand().FaktureSprzedazy_Dodaj(tra,sesja);
                DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentFs);

                com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
                var sesja2 = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
                tra.ExportKod = ImportowanyTypDokumentu.Paragon;
                var dokumentFs2 = new DokumentCommand().Paragon_Dodaj(tra, sesja2);
                com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);

        }

        [TestMethod]
        public void RezerwacjaOdbiorcy()
        {
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

            var tra = ToolsForTest.UtworzTransakcje(ImportowanyTypDokumentu.RezerwacjaOdbiorcy, "126/RO/2017");
            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR", 2);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR", 2);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR", 2);
            tra.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();

            tra.PozycjeDokumentuImportowanego.Add(pozycja1);
            tra.PozycjeDokumentuImportowanego.Add(pozycja2);
            tra.PozycjeDokumentuImportowanego.Add(pozycja3);

            tra.ExportKod = ImportowanyTypDokumentu.RezerwacjaOdbiorcy;
            tra.CzyZapisBufor = 0;
            var dokumentRo = new DokumentCommand().RezerwacjeOdbiorcy_Dodaj(tra, sesja);
            //DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentRo);


        }
        [TestMethod]
        public void MM_Dodaj()
        {

            new com.asoft.OptimaLib.Service.OptimaSessionPool();
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();

            var traMM = ToolsForTest.UtworzTransakcje("MM", "126/MM/2017");
            traMM.MagazynDocelowy = "MAG_DOCELOWY";

            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR-PZ", 3);

            ToolsForTest.UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traMM.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traMM.PozycjeDokumentuImportowanego.Add(pozycja1);
            traMM.PozycjeDokumentuImportowanego.Add(pozycja2);
            traMM.PozycjeDokumentuImportowanego.Add(pozycja3);

            var traPz = ToolsForTest.UtworzTransakcje("PZ", "126/PZ-MM/2017");
            traPz.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traPz.PozycjeDokumentuImportowanego.Add(pozycja1);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja2);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja3);
            traPz.ExportKod = ImportowanyTypDokumentu.PrzyjecieZewnetrzne;

            traMM.ExportKod = ImportowanyTypDokumentu.PrzesumiecieMM;
            traMM.CzyZapisBufor = 0;

            var dokumentPz = new DokumentCommand().PZ_Dodaj(traPz, sesja);

            dokumentPz.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traPz);

            var dokumentMM = new DokumentCommand().MM_Dodaj(traMM,sesja);

       

            var bruttoMM = dokumentMM.RazemBrutto;
            var nettoMM = dokumentMM.RazemNetto;

            IDokumentHaMag dokumentOptima = DokumentyCommonTools.ZnajdzDokumentPoTrNiD(sesja, dokumentMM.ID);

            var bruttoOptima = dokumentOptima.RazemBrutto;
            var nettoOptima = dokumentOptima.RazemNetto;

            Assert.AreEqual(18M, dokumentOptima.RazemNetto);
            Assert.AreEqual(dokumentMM.NumerPelny, dokumentOptima.NumerPelny);
            dokumentMM.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traMM);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentMM);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
        }

        [TestMethod]
        public void WZ_Dodaj()
        {

            new com.asoft.OptimaLib.Service.OptimaSessionPool();
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();


            string opisWZ = "126/WZ/2017";
            var traWz = ToolsForTest.UtworzTransakcje("WZ", opisWZ);

            traWz.MagazynDocelowy = "MAGAZYN";
            var pozycja1 = ToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = ToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = ToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR-PZ", 3);

            ToolsForTest.UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traWz.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traWz.PozycjeDokumentuImportowanego.Add(pozycja1);
            traWz.PozycjeDokumentuImportowanego.Add(pozycja2);
            traWz.PozycjeDokumentuImportowanego.Add(pozycja3);

            var traPz = ToolsForTest.UtworzTransakcje("PZ", "126/PZ-WZ/2017");
            traPz.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traPz.PozycjeDokumentuImportowanego.Add(pozycja1);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja2);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja3);

            var dokumentPz = new DokumentCommand().PZ_Dodaj(traPz,sesja);
           
            dokumentPz.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traPz);

            var dokumentWz = new DokumentCommand().WZ_Dodaj(traWz,sesja);
            

            IDokumentHaMag dokumentOptima = DokumentyCommonTools.ZnajdzDokumentPoTrNiD(sesja, dokumentWz.ID);

            Assert.AreEqual(321M, dokumentOptima.RazemBrutto);
            Assert.AreEqual(dokumentWz.NumerPelny, dokumentOptima.NumerPelny);
            dokumentWz.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traWz);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentWz);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
        }




        [TestMethod]
        public void RW_Dodaj()        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();

            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();


            var traRW = ToolsForTest.UtworzTransakcje("RW", "126/RW/2017");

            traRW.MagazynDocelowy = "MAGAZYN";
            var pozycja1 = ToolsForTest.UtworzPozycje(1M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = ToolsForTest.UtworzPozycje(2M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = ToolsForTest.UtworzPozycje(3M, 3M, "TOWAR-PZ", 3);

            ToolsForTest.UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traRW.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traRW.PozycjeDokumentuImportowanego.Add(pozycja1);
            traRW.PozycjeDokumentuImportowanego.Add(pozycja2);
            traRW.PozycjeDokumentuImportowanego.Add(pozycja3);


            var traPz = ToolsForTest.UtworzTransakcje("PZ", "126/PZ-RW/2017");
            traPz.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traPz.PozycjeDokumentuImportowanego.Add(pozycja1);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja2);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja3);
            var dokumentPz = new DokumentCommand().PZ_Dodaj(traPz,sesja);
            
            dokumentPz.Bufor = 0;

            CommonToolsCommand.ZapiszSesje(sesja, traPz);

            var dokumentRW = new DokumentCommand().RW_Dodaj(traRW,sesja);
            

            var bruttoRW = dokumentRW.RazemBrutto;
            var nettoRW = dokumentRW.RazemNetto;

            IDokumentHaMag dokumentOptima = DokumentyCommonTools.ZnajdzDokumentPoTrNiD(sesja, dokumentRW.ID);

            var bruttoOptima = dokumentOptima.RazemBrutto;
            var nettoOptima = dokumentOptima.RazemNetto;

            Assert.AreEqual(304000, dokumentOptima.Rodzaj);
            Assert.AreEqual("RW", dokumentOptima.TypDokumentuString);
            //testy ręczne działają - wartośc dokumentu jest 95
            //Assert.AreEqual(95M, brutto);
            Assert.AreEqual(dokumentRW.NumerPelny, dokumentOptima.NumerPelny);

            dokumentRW.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traRW);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentRW);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
        }



        [TestMethod]
        public void PZ_Dodaj()
        {

            new com.asoft.OptimaLib.Service.OptimaSessionPool();
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();


            var traPz = ToolsForTest.UtworzTransakcje("PZ", "126/PZ/2017");
            var pozycja1 = ToolsForTest.UtworzPozycje(1.23M, 100M, "TOWAR-PZ", 3);
            var pozycja2 = ToolsForTest.UtworzPozycje(12.3M, 10M, "TOWAR-PZ", 3);
            var pozycja3 = ToolsForTest.UtworzPozycje(123M, 1M, "TOWAR-PZ", 3);
            pozycja1.Lp = "1";
            pozycja2.Lp = "2";
            pozycja3.Lp = "3";

            pozycja1.DstId = 1;
            pozycja2.DstId = 2;
            pozycja3.DstId = 3;

            traPz.PozycjeDokumentuImportowanego = new List<ImportDokumentPozycjaDto>();
            traPz.PozycjeDokumentuImportowanego.Add(pozycja1);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja2);
            traPz.PozycjeDokumentuImportowanego.Add(pozycja3);
         

            var dokumentPz = new DokumentCommand().PZ_Dodaj(traPz,sesja);
           

            dokumentPz.Bufor = 0;
            CommonToolsCommand.ZapiszSesje(sesja, traPz);

            IDokumentHaMag dokumentOptima = DokumentyCommonTools.ZnajdzDokumentPoTrNiD(sesja, dokumentPz.ID);
            Assert.AreEqual(300, dokumentOptima.RazemBrutto);
            Assert.AreEqual(dokumentPz.NumerPelny, dokumentOptima.NumerPelny);
            DokumentyCommandAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
            com.asoft.OptimaLib.Service.OptimaSessionPool.PutSession(sesja);
        }





	}
}
