﻿
namespace com.asoft.OptimaLib.dtoOptima
{
    public enum OptimaTypSubElementu
    {
        //3 rezerwacja 2 pobierz
        /* Typ subelementów podpiętych do transakcji (rezerwacyjne lub wpływające na stany)
            1 - dostawa 
            2 - rozchód 
            3 - rezerwacja 
            4 - zamówienie
        */
        Dostawa = 1,
        Rozchod = 2,
        Rezerwacja = 3,
        Zamowienie = 4
    }

}
