using System.Web.Http;

namespace RestApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SwaggerConfig.Register();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new ApiKeyHandler(new InMemoryGetAllApiKeysQuery()));
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
