﻿using System;
using System.Collections.Generic;
using CDNBase;
using CDNHeal;
using com.asoft.OptimaLib.dtoService;

namespace OptimaSDKLib.Base
{
    public class KontrahenciToolsCommand
    {


        public static IKontrahent Dostawca_Pobierz(AdoSession sesja, ImportDokumentDto tra)
        {

            String P_Kod_Kontrahenta;

            IKontrahent Dostawca = null;

            P_Kod_Kontrahenta = UstawKodJesliPustyToNieokreslony(tra.KodDostawcy);

            ICollection Kontrahenci = (ICollection)(sesja.CreateObject("CDN.Kontrahenci", null));

            try
            {
                Dostawca = (IKontrahent)Kontrahenci[P_Kod_Kontrahenta];
            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - ustawić danych o dostawcy ", nameof(KontrahenciToolsCommand), e);
                throw new InvalidOperationException("Błąd - ustawić danych o dostawcy ", e);
            }


            return Dostawca;
        }


        private static string UstawKodJesliPustyToNieokreslony(String kodKontrahenta)
        {

            string P_Kod_Kontrahenta;
            if (!String.IsNullOrEmpty(kodKontrahenta) && !"JEDNORAZOWY".Equals(kodKontrahenta))
            {
                P_Kod_Kontrahenta = "Knt_Kod=" + "'" + kodKontrahenta + "'";
            }
            else
            {
                P_Kod_Kontrahenta = "Knt_Kod=" + "'" + "!NIEOKREŚLONY!" + "'";
            }

            return P_Kod_Kontrahenta;
        }

        public static IKontrahent Platnik_Pobierz(AdoSession sesja, ImportDokumentDto tra)
        {

            IKontrahent Platnik = null;

            String P_Kod_Platnika = UstawKodJesliPustyToNieokreslony(tra.KodPlatnika);

            ICollection Kontrahenci = (ICollection)(sesja.CreateObject("CDN.Kontrahenci", null));

            try
            {
                Platnik = (IKontrahent)Kontrahenci[P_Kod_Platnika];
            }
            catch (Exception bladPlatnika)
            {
                CommonToolsCommand.ZalogujBlad("Błąd - podczas wyszukiwania danych o płatniku: " + P_Kod_Platnika, nameof(KontrahenciToolsCommand), bladPlatnika);
                throw new InvalidOperationException("Błąd - podczas wyszukiwania danych o płatniku: " + P_Kod_Platnika,  bladPlatnika);
            }
            if (Platnik == null)
            {
                var exception = new InvalidOperationException("Błąd - nie udało się ustawić danych o płatniku: " + P_Kod_Platnika);
                CommonToolsCommand.ZalogujBlad("Błąd - nie udało się ustawić danych o płatniku: " + P_Kod_Platnika, nameof(KontrahenciToolsCommand), exception);
                throw exception;
            }
            return Platnik;
        }





        public static IKontrahent ZnajdzKontrahenta(AdoSession Sesja, ImportDokumentDto tra)
        {
            string P_Kod_Kontrahenta;

            P_Kod_Kontrahenta = UstawKodJesliPustyToNieokreslony(tra.KodOdbiorcy);

            CDNBase.ICollection Kontrahenci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Kontrahenci", null));
            CDNHeal.IKontrahent Kontrahent = null;
            try
            {
                Kontrahent = (CDNHeal.IKontrahent)Kontrahenci[P_Kod_Kontrahenta];
            }
            catch
            {
                if ("!NIEOKREŚLONY!".Equals(P_Kod_Kontrahenta))
                {
                    var exceptionNieokreslony = new System.InvalidOperationException(
                        "Błąd - kod kontrahenta nie odnaleziony w Optimie: !NIEOKREŚLONY!");
                    CommonToolsCommand.ZalogujBlad("Błąd - kod kontrahenta nie odnaleziony w Optimie: !NIEOKREŚLONY!", nameof(KontrahenciToolsCommand), exceptionNieokreslony);

                    throw exceptionNieokreslony;
                }

                var exception = new System.InvalidOperationException("Błąd - kod kontrahenta nie odnaleziony w Optimie:" + P_Kod_Kontrahenta);
                CommonToolsCommand.ZalogujBlad("Błąd - kod kontrahenta nie odnaleziony w Optimie: !NIEOKREŚLONY!", nameof(KontrahenciToolsCommand), exception);

                throw exception;
            }
            return Kontrahent;
        }

        public static IKontrahent ZnajdzOdbiorce(AdoSession sesja, ImportDokumentDto tra)
        {
            string P_Kod_Odbiorcy = UstawKodJesliPustyToNieokreslony(tra.KodOdbiorcy);
            CDNBase.ICollection Odbiorcy = (CDNBase.ICollection)(sesja.CreateObject("CDN.Kontrahenci", null));
            CDNHeal.IKontrahent Odbiorca = null;
            try
            {
                Odbiorca = (CDNHeal.IKontrahent)Odbiorcy[P_Kod_Odbiorcy];
            }
            catch (Exception bladPlatnika)
            {
                if ("!NIEOKREŚLONY!".Equals(P_Kod_Odbiorcy))
                {
                    var exceptionNieokreslony = new System.InvalidOperationException(
                       "Błąd - kod kontrahenta nie odnaleziony w Optimie: !NIEOKREŚLONY!");
                    CommonToolsCommand.ZalogujBlad(exceptionNieokreslony.Message, nameof(KontrahenciToolsCommand), exceptionNieokreslony);
                    throw exceptionNieokreslony;
                }
                else
                {                   
                    var exceptionOdbiorcy = new System.InvalidOperationException("Błąd - kod odbiorcy nie odnaleziony w Optimie:" + P_Kod_Odbiorcy);
                    CommonToolsCommand.ZalogujBlad(exceptionOdbiorcy.Message, nameof(KontrahenciToolsCommand), exceptionOdbiorcy);

                    throw exceptionOdbiorcy;
                }
            }
            return Odbiorca;
        }


    }   

}