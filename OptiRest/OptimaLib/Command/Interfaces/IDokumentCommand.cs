﻿using CDNBase;
using CDNHlmn;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Base
{
    public interface IDokumentCommand
    {
        List<DokumentDto> ListaDokumentowOptimaNieAnulowanych(DokumentyKryteriaDto dokumentyKryteriaDto, AdoSession sesja);
        IDokumentHaMag AnulujDokumentOptimaPoNumerzePelnym(AdoSession sesja, string nrDokumentu);

        IDokumentHaMag PZ_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);

        IDokumentHaMag WZ_Dodaj(ImportDokumentDto dto,AdoSession Sesja);
        IDokumentHaMag Paragon_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);

        IDokumentHaMag FaktureSprzedazy_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag PKA_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag MM_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag MMPlus_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
     
        IDokumentHaMag RW_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag PW_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag WKA_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag FZ_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);
        IDokumentHaMag WZ_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO);

        IDokumentHaMag PZ_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO);
        IDokumentHaMag FS_PA_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO);
        IDokumentHaMag Rezerwacje_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja);





    }
}
