﻿using com.asoft.OptimaLib.dtoOptima;
using System;

namespace com.asoft.OptimaLib.dtoCriteria
{
    public class ZasobyKryteriaDto
    {
        public string KodTowaru { get; set; }
        public string NazwaTowaru { get; set; }

        public string  Mag_Symbol { get; set; }
        public DateTime Data { get; set; }

        public string ToExceptionString()
        {
            return "Kryteria: KodTowaru =" + KodTowaru + " NazwaTowaru = " + NazwaTowaru + " OstatniaModyfikacja =" + Data + " Mag_Symbol = " + Mag_Symbol+Environment.NewLine;
        }
    }
    
}
