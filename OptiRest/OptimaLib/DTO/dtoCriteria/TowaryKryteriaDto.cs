﻿using com.asoft.OptimaLib.dtoOptima;
using System;

namespace com.asoft.OptimaLib.dtoCriteria
{
    public class TowaryKryteriaDto
    {
        public string KodTowaru { get; set; }
        public string NazwaTowaru { get; set; }
        public DateTime OstatniaModyfikacja { get; set; }

        public string ToExceptionString()
        {
            return "Kryteria: KodTowaru =" + KodTowaru + " NazwaTowaru = " + NazwaTowaru + " OstatniaModyfikacja =" + OstatniaModyfikacja + Environment.NewLine;
        }
    }
    
}
