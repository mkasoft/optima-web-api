﻿using System;

namespace com.asoft.OptimaLib.helpers
{
    public class DateConverter
    {
        static System.Globalization.DateTimeFormatInfo dti = new System.Globalization.DateTimeFormatInfo();
        static string dateFormat = "yyyyMMdd HH:mm:ss";
        static string dateFormatShort = "yyyyMMdd";
        //convert DateTime into Formated String
        public static string convertDateTimeToFormatedString(DateTime dateTime)
        {   
            return dateTime.ToString(dateFormat, dti);
        }

        public static string convertDateTimeToFormatedStringWithoutTime(DateTime dateTime)
        {
            return dateTime.ToString(dateFormatShort, dti);
        }


        //convert Formatted String into DateTime
        public static DateTime convertStringToDateTime(string dateTimeInString)
        {
            dti.LongTimePattern = dateFormat;
            return DateTime.ParseExact(dateTimeInString, "T", dti);
        }
    }
}