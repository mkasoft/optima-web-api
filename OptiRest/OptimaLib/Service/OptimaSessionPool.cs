﻿using System.Configuration;
using CDNBase;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib.Service
{
    public class OptimaSessionPool
    {

        protected static ILogin Login { get; private set; }
        protected static LogowanieCommand logowanie = new LogowanieCommand();
        static string _user;
        static string _haslo;
        static string _firma;
        public string path;


        public OptimaSessionPool()
        {
          }

        private static SessionPool<AdoSession> _pool = new SessionPool<AdoSession>(() =>
        {
            _user = ConfigurationSettings.AppSettings["user"];
            _haslo = ConfigurationSettings.AppSettings["haslo"];
            _firma = ConfigurationSettings.AppSettings["firma"];

            logowanie.Zaloguj(_user, _haslo, _firma);
            Login = logowanie.Login;
            AdoSession sesja = Login.CreateSession();
            return sesja;
        }, 1, 1);

        public static AdoSession GetSession()
        {
            return _pool.GetObject();
        }

        public static void PutSession(AdoSession session)
        {
            _pool.PutObject(session);
        }

        public static AdoSession RecreateNewSession(AdoSession sesja)
        {
            logowanie.Wyloguj();
            logowanie.Zaloguj(_user, _haslo, _firma);
            Login = logowanie.Login;        
            return Login.CreateSession(); 

        }
    }
}
