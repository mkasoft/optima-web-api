﻿
using CDNBase;
using CDNHeal;
using CDNHlmn;
using com.asoft.Base;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoOptima;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.helpers;
using OptimaSDKLib.Base;
using System;
using System.Collections.Generic;

namespace com.asoft.OptimaLib.Base
{
    public class DokumentCommand : IDokumentCommand
    {
        const string GOTOWKA = "gotówka";
        private const int DOKUMENT_BRUTTO = 2;
        private const int DOKUMENT_NETTO = 1;
       

        private ImportDokumentDto _impTranDTO { get; set; }

        public DokumentCommand()        {
            
        }

        public  IDokumentHaMag AnulujDokumentOptimaPoNumerzePelnym(AdoSession sesja, string nrDokumentu)
        {
            CDNHlmn.IDokumentHaMag dokumentOptima;
            try
            {
                if (sesja == null)
                {
                    throw new System.InvalidOperationException(" Błąd - Nie można utworzyć sesji do systemu Comarch.");
                }
                dokumentOptima = DokumentyCommonTools.ZnajdzDokumentOptimaNieAnulowanyPoNumerzePelnym(sesja, nrDokumentu);
                AnulujDokumentOptima(sesja, dokumentOptima);
            }
            catch (Exception ex)
            {
                throw new System.InvalidOperationException("Błąd: Nie udało się anulować dokumentu w Optimie " + nrDokumentu + " " + ex.Message);
            }
            return dokumentOptima;
        }


        public List<DokumentDto> ListaDokumentowOptimaNieAnulowanych( DokumentyKryteriaDto dokumentyKryteriaDto, AdoSession sesja)
        {

            String queryString = "";
            if (dokumentyKryteriaDto.DataPoczatek > DateTime.MinValue)
            {
                queryString += " and Trn_DataWys >='" + DateConverter.convertDateTimeToFormatedStringWithoutTime(dokumentyKryteriaDto.DataPoczatek) + "'";
            }

            if (dokumentyKryteriaDto.DataKoniec < DateTime.MaxValue && dokumentyKryteriaDto.DataKoniec > DateTime.MinValue)
            {
                queryString += " and Trn_DataWys <='" + DateConverter.convertDateTimeToFormatedStringWithoutTime(dokumentyKryteriaDto.DataKoniec) + "'";
            }

            if (dokumentyKryteriaDto.RodzajDokumentu != null)
            {
                queryString += " and Trn_Rodzaj = " + dokumentyKryteriaDto.RodzajDokumentu;
            }

            if (dokumentyKryteriaDto.TypDokumentu != null)
            {
                queryString += " and Trn_TypDokumentu = " + dokumentyKryteriaDto.TypDokumentu;
            }

            if (dokumentyKryteriaDto.NrDokumentu != null)
            {
                queryString += " and   TrN_NumerPelny = '" + dokumentyKryteriaDto.NrDokumentu + "'";
            }



            ADODB.Connection connection = sesja.Connection;
            ADODB.Recordset rRs = new ADODB.Recordset();
            string select = "select top 1000 TrN_TrNId as ID from cdn.TraNag where Trn_Anulowany = 0 " + queryString;

            rRs.Open(select, connection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);

            System.Collections.Generic.List<DokumentDto> listaDokumentDto = new List<DokumentDto>();
            DokumentyHaMag dokumenty = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            
            CommonToolsCommand.ZalogujWiadomosc("Wykonano zapytanie: " + select + " znaleziono = " + rRs.RecordCount + " rekordy", nameof(DokumentCommand));

            while (rRs.EOF == false)
            {
                DokumentHaMag optimaDok = dokumenty["TrN_TrNID=" + rRs.Fields["ID"].Value];
                DokumentDto dokumentDto = new DokumentDto
                {
                    Id = optimaDok.ID,
                    DataWyst = optimaDok.DataWys,
                    DataDok = optimaDok.DataDok,
                    NumerPelny = optimaDok.NumerPelny,
                    MagazynZrodlowy = optimaDok.MagazynZrdNazwa,
                    Rodzaj = optimaDok.Rodzaj,
                    Vat = optimaDok.RazemVAT,
                    Netto = optimaDok.RazemNetto,
                    Brutto = optimaDok.RazemBrutto
                };
                if (optimaDok.MagazynDocelowyID != 0)
                {
                    dokumentDto.MagazynDocelowy = optimaDok.MagazynDocNazwa;
                }

                if (optimaDok.Podmiot != null)
                {
                    dokumentDto.PodmiotAkronim = optimaDok.Podmiot.Akronim;
                }
                
                if (optimaDok.Elementy != null)
                {
                    dokumentDto.Pozycje = new List<DokumentPozycjaDto>();
                    foreach (IElementHaMag pozycjeOptimy in optimaDok.Elementy)
                    {
                        DokumentPozycjaDto pozycjaDto = new DokumentPozycjaDto
                        {
                            Lp = pozycjeOptimy.Lp,
                            MagID = pozycjeOptimy.MagID,
                            TowarID = pozycjeOptimy.TowarID,
                            TowarKod = pozycjeOptimy.TowarKod,
                            TowarNazwa = pozycjeOptimy.TowarNazwa,
                            JM = pozycjeOptimy.JM,
                            Ilosc = pozycjeOptimy.Ilosc,
                            Cena = pozycjeOptimy.Cena0WD,
                            Wartosc = pozycjeOptimy.Wartosc,
                            WartoscZakupu = pozycjeOptimy.WartoscZakupu
                        };

                        dokumentDto.Pozycje.Add(pozycjaDto);
                    }
                }

                listaDokumentDto.Add(dokumentDto);
                rRs.MoveNext();
            }

            return listaDokumentDto;
        }


        public IDokumentHaMag Paragon_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)

        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.Paragon, impTranDTO);

            IDokumentHaMag dokumentOptima = DokumentHandlowyOptimaNowy(sesja);
            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);
            UstawPodmiotIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);
            UstawOdbiorceIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);
            dokumentOptima.TypSubElementu = (int)OptimaTypSubElementu.Zamowienie; ;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokumentOptima.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokumentOptima);

            //Ustawiamy formę płatności
            dokumentOptima.FormaPlatnosci = formaPlatnosci;
            if (!string.IsNullOrEmpty(_impTranDTO.RachunekAkronim))
            {
                try
                {
                    dokumentOptima.FormaPlatnosci.RachunekAkronim = _impTranDTO.RachunekAkronim;
                }
                catch
                {
                    throw new InvalidOperationException("Błąd - Brak rachunku : " + _impTranDTO.RachunekAkronim);
                }
            }
            dokumentOptima.NumerObcy = _impTranDTO.NumerObcy;
            dokumentOptima.Uwagi = _impTranDTO.Opis;
            dokumentOptima.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_FS_PA(sesja, dokumentOptima, _impTranDTO);
            dokumentOptima.BlokadaPlatnosci = 0;
            dokumentOptima.BlokadaGeneracjiPlatnosciPrzyZapisie(0);
            // Przestawienie formy platnosci na gotówkę 
            // Uwaga - musi istnieć otwarty raport kasaowy do którego mógł by być wpisany zapis kasowy
            dokumentOptima.FormaPlatnosci = formaPlatnosci;
            // Ustawienie flag generacji automatycznej platności 
            dokumentOptima.PlatnoscDokumentu.BlokadaGeneracjiPlatnosciPrzyZapisie = 0;
            dokumentOptima.PlatnoscDokumentu.RozliczeniaAutomatyczne = 0;

            DokumentyCommonTools.UstawDateDokumentu(dokumentOptima, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);

            if (GOTOWKA.Equals(formaPlatnosci.Nazwa) && formaPlatnosci.Termin == 0)

            {
                DokumentyCommonTools.BlokadaPlatnosci_Dokument(sesja, dokumentOptima.ID);
            }

            return dokumentOptima;

        }
        public IDokumentHaMag FaktureSprzedazy_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)

        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.FakturaSprzedazy, impTranDTO);

            IDokumentHaMag dokumentOptima = DokumentHandlowyOptimaNowy(sesja);

            //Ustalamy ormę płatności
            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);

            UstawPodmiotIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);
            UstawOdbiorceIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);

            dokumentOptima.TypSubElementu = (int)OptimaTypSubElementu.Zamowienie;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokumentOptima.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokumentOptima);

            //Ustawiamy formę płatności
            dokumentOptima.FormaPlatnosci = formaPlatnosci;
            if (!string.IsNullOrEmpty(_impTranDTO.RachunekAkronim))
            {
                try
                {
                    dokumentOptima.FormaPlatnosci.RachunekAkronim = _impTranDTO.RachunekAkronim;
                }
                catch
                {
                    throw new InvalidOperationException("Błąd - Brak rachunku : " + _impTranDTO.RachunekAkronim);
                }
            }
            dokumentOptima.NumerObcy = _impTranDTO.NumerObcy;
            dokumentOptima.Uwagi = _impTranDTO.Opis;
            dokumentOptima.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_FS_PA(sesja, dokumentOptima, _impTranDTO);
            dokumentOptima.BlokadaPlatnosci = 1;
            dokumentOptima.PlatnoscDokumentu.BlokadaGeneracjiPlatnosciPrzyZapisie = 1;


            DokumentyCommonTools.UstawDateDokumentu(dokumentOptima, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);

            return dokumentOptima;

        }

        public IDokumentHaMag RezerwacjeOdbiorcy_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja)

        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.RezerwacjaOdbiorcy, impTranDTO);

            IDokumentHaMag dokumentOptima = DokumentHandlowyOptimaNowy(sesja);

            //Ustalamy ormę płatności
            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);

            UstawPodmiotIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);
            UstawOdbiorceIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);

            dokumentOptima.TypSubElementu = (int)OptimaTypSubElementu.Zamowienie;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokumentOptima.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokumentOptima);

            //Ustawiamy formę płatności
            dokumentOptima.FormaPlatnosci = formaPlatnosci;
            if (!string.IsNullOrEmpty(_impTranDTO.RachunekAkronim))
            {
                try
                {
                    dokumentOptima.FormaPlatnosci.RachunekAkronim = _impTranDTO.RachunekAkronim;
                }
                catch
                {
                    throw new InvalidOperationException("Błąd - Brak rachunku : " + _impTranDTO.RachunekAkronim);
                }
            }
            dokumentOptima.NumerObcy = _impTranDTO.NumerObcy;
            dokumentOptima.Uwagi = _impTranDTO.Opis;
            dokumentOptima.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            dokumentOptima.NumerPelnyPrw= impTranDTO.NumerPelny;

            DodajAtrybutDoDOkumentu(sesja, dokumentOptima, "SODO_X", "X:Nr: XP123456");
            DodajAtrybutDoDOkumentu(sesja, dokumentOptima, "SODO_Y", "Y:Nr: XP123456");


            DokumentPozycjeCommand.Pozycje_FS_PA(sesja, dokumentOptima, _impTranDTO);
            dokumentOptima.BlokadaPlatnosci = 1;
            dokumentOptima.PlatnoscDokumentu.BlokadaGeneracjiPlatnosciPrzyZapisie = 1;


            DokumentyCommonTools.UstawDateDokumentu(dokumentOptima, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);

            return dokumentOptima;

        }

        private void DodajAtrybutDoDOkumentu(AdoSession sesja, IDokumentHaMag dokumentOptima , String kodAtrubutu, String wartosdcAtrybutu)
        {
            //Dodanie atrybutu dokumentu TEKST
            CDNBase.ICollection rAtrybuty = (CDNBase.ICollection)(sesja.CreateObject("CDN.DefAtrybuty", null));
            CDNTwrb1.IDefAtrybut rAtrybut = (CDNTwrb1.IDefAtrybut)rAtrybuty["dea_Kod = '"+ kodAtrubutu+"'"];
            CDNTwrb1.IDokAtrybut rAtrybutDokumentu = (CDNTwrb1.IDokAtrybut)dokumentOptima.Atrybuty.AddNew(null);
            rAtrybutDokumentu.DefAtrybut = (CDNTwrb1.DefAtrybut)rAtrybut;
            rAtrybutDokumentu.Wartosc = wartosdcAtrybutu;

            // Atrybut można też podstawić za pomocą ID atrybutu bez tworzenia kolekcji atrybutów:
            // rAtrybutDokumentu.DeAID = 123
        }

        public IDokumentHaMag PZ_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.PrzyjecieZewnetrzne, impTranDTO);

            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);

            //Dodajemy pozycje
            DokumentPozycjeCommand.Pozycje_PZ(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }



        public IDokumentHaMag PKA_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.PrzyjecieKaucji, impTranDTO);

            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);

            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;

            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_PKA(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }



        public IDokumentHaMag MM_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.PrzesumiecieMM, impTranDTO);

            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            IKontrahent odbiorca = KontrahenciToolsCommand.Dostawca_Pobierz(sesja, _impTranDTO);

            dokument.Podmiot = odbiorca;
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            dokument.MagazynDocelowy = DokumentyCommonTools.UstawMagazynDocelowy(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_MM(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }

        public IDokumentHaMag MMPlus_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.PrzesumiecieMM, impTranDTO);
            IDokumentHaMag dokument = DokumentyCommonTools.ZnajdzDokumentOptimyPoNumerPelny(sesja, _impTranDTO);

            DokumentPozycjeCommand.Pozycje_MMPlus(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }

        public IDokumentHaMag WZ_Dodaj(ImportDokumentDto impTranDTO,AdoSession Sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.WydanieZewnetrzne, impTranDTO);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(Sesja, _impTranDTO);
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(Sesja);
            dokument.Podmiot = platnik;
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);
            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;            
            DokumentPozycjeCommand.Pozycje_WZ(Sesja, dokument, _impTranDTO);

            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(Sesja, _impTranDTO);
            return dokument;
        }

        public IDokumentHaMag RW_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.RozchodWewnetrzny, impTranDTO);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_RW(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }

        public IDokumentHaMag PW_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.PrzyjecieWewnetrzne, impTranDTO);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_PW(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }
        public IDokumentHaMag WKA_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.WydanieKaucji, impTranDTO);
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;
            dokument.Bufor = _impTranDTO.CzyZapisBufor;

            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);

            DokumentPozycjeCommand.Pozycje_WKA(sesja, dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);


            return dokument;
        }
       
        public IDokumentHaMag FZ_Dodaj(ImportDokumentDto impTranDTO,AdoSession sesja)
        {
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.FakturaZakupu, impTranDTO);
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);

            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            dokument.Podmiot = platnik;
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);


            //Ustawiamy formę płatności

            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);
            dokument.FormaPlatnosci = formaPlatnosci;
            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_FZ(sesja, dokument, _impTranDTO);

            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }

        public IDokumentHaMag WZ_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO)
        {
            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            dokument.Podmiot = platnik;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);

            var dokumentKorygowany = DokumentyCommonTools.ZnajdzDokumentKorygowanyPoTrnId(sesja, _impTranDTO);
            dokument.OryginalID = dokumentKorygowany.ID;
            /*  Faktura korygująca
              Określa, czy faktura jest fakturą korygującą, oraz typ korekty: 
              0 - faktura zwykła 
              1 - korekta ilości 
              2 - korekta wartości (ceny) 
              3 - korekta Vat 
              4 - nota korygująca 
              5 - korekta graniczna 
              6 - korekta graniczna UE
          */
            //Rodzaj korekty
            dokument.Korekta = 1;
            DokumentPozycjeCommand.Zmien_Pozycje(dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }



        public IDokumentHaMag PZ_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO)
        {

            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);
            IKontrahent platnik = KontrahenciToolsCommand.Platnik_Pobierz(sesja, _impTranDTO);
            //Ustawiamy podmiot
            dokument.Podmiot = platnik;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;
            //  if (Dokument.TypDokumentu == 304) Dokument.Bufor = 0;
            UstawDokumentBruttoNetto(dokument);

            dokument.Uwagi = _impTranDTO.Opis;
            dokument.NumerObcy = _impTranDTO.NumerObcy;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            var dokumentKorygowany = DokumentyCommonTools.ZnajdzDokumentKorygowany(sesja, _impTranDTO);
            dokument.OryginalID = dokumentKorygowany.ID;

            /*  Faktura korygująca
                 Określa, czy faktura jest fakturą korygującą, oraz typ korekty: 
                 0 - faktura zwykła 
                 1 - korekta ilości 
                 2 - korekta wartości (ceny) 
                 3 - korekta Vat 
                 4 - nota korygująca 
                 5 - korekta graniczna 
                 6 - korekta graniczna UE
                 */

            if ("1".Equals(_impTranDTO.CzyKorektaCeny))
            {
                dokument.Korekta = 2;
                DokumentPozycjeCommand.Zmien_Pozycje_KorektaCeny(sesja, dokument, _impTranDTO);
            }
            else
            {
                dokument.Korekta = 1;
                DokumentPozycjeCommand.Zmien_Pozycje(dokument, _impTranDTO);
            }

            if (!string.IsNullOrEmpty(dokumentKorygowany.NumerObcy))
            {
                dokument.NumerObcy = _impTranDTO.Opis;
            }
            else
            {
                dokument.NumerObcy = _impTranDTO.Opis;
            }
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokument;
        }



        public IDokumentHaMag FS_PA_DogenerujKorekte(AdoSession sesja, ImportDokumentDto _impTranDTO)
        {

            IDokumentHaMag dokument = DokumentHandlowyOptimaNowy(sesja);

            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);
            UstawPodmiotIUstawFormePlatnosci(sesja, dokument, formaPlatnosci);
            UstawOdbiorceIUstawFormePlatnosci(sesja, dokument, formaPlatnosci);
            dokument.TypSubElementu = (int)OptimaTypSubElementu.Zamowienie; ;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokument.Bufor = _impTranDTO.CzyZapisBufor;

            UstawDokumentBruttoNetto(dokument);

            //Ustawiamy formę płatności 
            dokument.FormaPlatnosci = formaPlatnosci;
            if (!string.IsNullOrEmpty(_impTranDTO.RachunekAkronim))
            {
                dokument.FormaPlatnosci.RachunekAkronim = _impTranDTO.RachunekAkronim;
            }
            dokument.NumerObcy = _impTranDTO.NumerPelny;
            dokument.Uwagi = _impTranDTO.Opis;
            dokument.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);

            /*  Faktura korygująca
              Określa, czy faktura jest fakturą korygującą, oraz typ korekty: 
              0 - faktura zwykła 
              1 - korekta ilości 
              2 - korekta wartości (ceny) 
              3 - korekta Vat 
              4 - nota korygująca 
              5 - korekta graniczna 
              6 - korekta graniczna UE
              */
            //Rodzaj korekty
            dokument.Korekta = 1;

            var dokumentKorygowany = DokumentyCommonTools.ZnajdzDokumentKorygowanyNieanulowany(sesja, _impTranDTO);
            dokument.OryginalID = dokumentKorygowany.ID;
            DokumentPozycjeCommand.Zmien_Pozycje(dokument, _impTranDTO);
            DokumentyCommonTools.UstawDateDokumentu(dokument, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            if (ImportowanyTypDokumentu.Paragon.Equals(_impTranDTO.ExportKod))
            {

                if (GOTOWKA.Equals(formaPlatnosci.Nazwa) && formaPlatnosci.Termin == 0)
                {
                    {
                        DokumentyCommonTools.BlokadaPlatnosci_Dokument(sesja, dokument.ID);
                    }
                }
            }

            return dokument;
        }

        private void Dogenerowanie_WZ_Do_FA(AdoSession sesja, decimal ID)
        {
            ADODB.Connection connection = sesja.Connection;
            ADODB.Recordset rRs = new ADODB.Recordset();
            string select = "select top 1 TrN_TrNId as ID from cdn.TraNag where TrN_TrNId =  " + ID;
            rRs.Open(select, connection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);
            // Utowrzenie obiektu serwisu:
            SerwisHaMag Serwis = (SerwisHaMag)sesja.CreateObject("Cdn.SerwisHaMag", null);
            // Samo gemerowanie dok. FA
            int MAGAZYN_ID = 1;
            Serwis.AgregujDokumenty(302, rRs, MAGAZYN_ID, 306, null);
            //Zapisujemy
            sesja.Save();
        }

        private void UstawPodmiotIUstawFormePlatnosci(AdoSession sesja, IDokumentHaMag dokumentOptima, OP_KASBOLib.FormaPlatnosci formaPlatnosci)
        {
            //Podmiot w Optimie
            var kontrahentOptima = KontrahenciToolsCommand.ZnajdzKontrahenta(sesja, _impTranDTO);
            dokumentOptima.Podmiot = kontrahentOptima;
            kontrahentOptima.FormaPlatnosci = formaPlatnosci;
        }

        private void UstawOdbiorceIUstawFormePlatnosci(AdoSession sesja, IDokumentHaMag dokumentOptima, OP_KASBOLib.FormaPlatnosci formaPlatnosci)
        {
            //Odbiorca w Optimie
            var odbiorcaOptima = KontrahenciToolsCommand.ZnajdzOdbiorce(sesja, _impTranDTO);
            dokumentOptima.Odbiorca = odbiorcaOptima;
            odbiorcaOptima.FormaPlatnosci = formaPlatnosci;
        }

        private IDokumentHaMag DokumentHandlowyOptimaNowy(AdoSession sesja)
        {
            var dokument = DodajPustyDokumentOptimy(sesja, _impTranDTO);
            DokumentyCommonTools.SprawdzCzyDokumentJuzIstnieje(sesja, _impTranDTO, dokument);
            DokumentyCommonTools.OkreslKategorieDokumentu(sesja, _impTranDTO, dokument);
            return dokument;
        }

        private static IDokumentHaMag DodajPustyDokumentOptimy(AdoSession sesja, ImportDokumentDto _impTranDTO)
        {
            var definicjaDokumentu = DokumentyCommonTools.ZnajdzDefninicjeDokumentuOptimy(sesja, _impTranDTO);
            var _dokumentyHaMagOptima = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag", null);
            IDokumentHaMag dokument = null;

            try
            {
                dokument = (IDokumentHaMag)_dokumentyHaMagOptima.AddNew(null);
                dokument.Rodzaj = definicjaDokumentu.Klasa * 1000;
                dokument.TypDokumentu = definicjaDokumentu.Klasa;
                DokumentyCommonTools.ZamienRodzajDokumentuDlaKorekty(_impTranDTO, dokument);

                object Numerator;
                // Ustawiamy numerator
                Numerator = dokument.Numerator;
                DokumentyCommonTools.SetProperty(Numerator, "DefinicjaDokumentu", definicjaDokumentu);
                if (!string.IsNullOrEmpty(_impTranDTO.SeriaDok))
                {
                    DokumentyCommonTools.SetProperty(Numerator, "Rejestr", _impTranDTO.SeriaDok);
                }
                var nrDokumentu = DokumentyCommonTools.WyodrebnijNrDokumentu(_impTranDTO);
                DokumentyCommonTools.SetProperty(Numerator, "NumerNr", nrDokumentu);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(
                    "Błąd - Nie można dodawać dokumentów do systemu Comarch - niedozwolona operacja. " + ex.Message);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new InvalidOperationException("Błąd - Nie można dodawać dokumentów do systemu Comarch. (SQL)" +
                                                           ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Błąd - Nie można dodawać dokumentów do systemu Comarch. " +
                                                           ex.Message);
            }
            return dokument;
        }

        private void UstawDokumentBruttoNetto(IDokumentHaMag dokument)
        {

            if (dokument.TypDokumentu == (int)OptimaTypDokumentu.WydanieZewnetrzne
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.WydanieKaucji
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.FakturaSprzedazy)
            {
                //WZ zawsze brutto
                //WKA zawsze brutto

                dokument.TypNB = DOKUMENT_BRUTTO;

            }
            else if (dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzyjecieZewnetrzne
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzesumiecieMM
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzyjecieKaucji
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.RozchodWewnetrzny
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.PrzyjecieWewnetrzne
                || dokument.TypDokumentu == (int)OptimaTypDokumentu.FakturaZakupu)
            {
                //PKA
                //PZ zawsze netto
                //MM zawsze netto
                //RW
                //FZ zawsze netto                
                dokument.TypNB = DOKUMENT_NETTO;
            }
            else
            {
                //defaul - konfiguration parameter candidate
                dokument.TypNB = DOKUMENT_BRUTTO;
            }

        }

        private void WalidujSprawdzTypDokumentu(string typDokumentu, ImportDokumentDto impTranDTO)
        {
            if (typDokumentu == null || !typDokumentu.Equals(impTranDTO.ExportKod))
            {
                throw new InvalidOperationException("Niewłaściwy typ dokumentu dla metody: " + _impTranDTO.ExportKod);
            }
        }
        private void WalidujImpTranDTOIZapamietajLokalnie(ImportDokumentDto impTranDTO)
        {
          
            if (impTranDTO.ExportKod == null)
            {
                throw new InvalidOperationException("Błąd - dane wejściowe nie są poprawne - pusty typ dokumentu");
            }
            var walidator = new ImportowanyTypDokumentu();
            walidator.JestObslugiwany(impTranDTO.ExportKod);           
            _impTranDTO = impTranDTO;
        }


        private IDokumentHaMag WZ_PA_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja)
        {

            IDokumentHaMag dokumentOptimaPowiazany = DokumentyCommonTools.ZnajdzDokumentPowiazanyPoOpisie(sesja, _impTranDTO);
            if (dokumentOptimaPowiazany != null)
            {
                Dogenerowanie_WZ_Do_FA(sesja, dokumentOptimaPowiazany.ID);
            }
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
            return dokumentOptimaPowiazany;
        }

        private void WZ_PA_Opis(ImportDokumentDto impTranDTO, AdoSession sesja)
        {

            IDokumentHaMag dokumentOptimaPowiazany = DokumentyCommonTools.ZnajdzDokumentPowiazanyPoOpisie(sesja, _impTranDTO);
            if (dokumentOptimaPowiazany != null)
            {
                if (string.IsNullOrEmpty(dokumentOptimaPowiazany.Uwagi))
                {
                    dokumentOptimaPowiazany.Uwagi = _impTranDTO.NumerPelny;
                }
                else
                {
                    dokumentOptimaPowiazany.Uwagi += " " + _impTranDTO.NumerPelny;
                }
            }
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);
        }
        private void WZ_PA_Opis(AdoSession sesja, ImportDokumentDto tra)
        {

            IDokumentHaMag dokumentOptimaPowiazany = DokumentyCommonTools.ZnajdzDokumentPowiazanyPoOpisie(sesja, tra);
            if (dokumentOptimaPowiazany != null)
            {
                if (string.IsNullOrEmpty(dokumentOptimaPowiazany.Uwagi))
                {
                    dokumentOptimaPowiazany.Uwagi = tra.NumerPelny;
                }
                else
                {
                    dokumentOptimaPowiazany.Uwagi += " " + tra.NumerPelny;
                }
            }
            CommonToolsCommand.ZapiszSesje(sesja, tra);
        }

        private static void AnulujDokumentOptima(AdoSession sesja, IDokumentHaMag dokumentOptima)
        {
            if (dokumentOptima.Bufor == 1)
            {
                throw new System.InvalidOperationException("Błąd: Dokument jest w buforze nie można go anulować" + dokumentOptima.NumerPelny);
            }
            if (dokumentOptima.CzyMoznaAnulowac() != 0)
            {
                dokumentOptima.AnulujDok(dokumentOptima.ID);
            }

            else
            {
                throw new System.InvalidOperationException("Błąd: Cechy dokumentu uniemożliwiają anulowanie. " + dokumentOptima.NumerPelny);
            }
            CommonToolsCommand.ZapiszSesje(sesja);
        }

        public IDokumentHaMag Rezerwacje_Dodaj(ImportDokumentDto impTranDTO, AdoSession sesja)
        {

            //TODO JKW clean it - crtl-c ctrl-v programming
            WalidujImpTranDTOIZapamietajLokalnie(impTranDTO);
            WalidujSprawdzTypDokumentu(ImportowanyTypDokumentu.RezerwacjaOdbiorcy, impTranDTO);

            IDokumentHaMag dokumentOptima = DokumentHandlowyOptimaNowy(sesja);

            //Ustalamy ormę płatności
            OP_KASBOLib.FormaPlatnosci formaPlatnosci = PlatnosciToolsCommand.ZnajdzFormePlatnosci(sesja, _impTranDTO);

            UstawPodmiotIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);
            UstawOdbiorceIUstawFormePlatnosci(sesja, dokumentOptima, formaPlatnosci);

            dokumentOptima.TypSubElementu = (int)OptimaTypSubElementu.Zamowienie;
            //Ustawiamy bufor 0 - zatwierdzona 1 w buforze
            dokumentOptima.Bufor = _impTranDTO.CzyZapisBufor;
            UstawDokumentBruttoNetto(dokumentOptima);

            //Ustawiamy formę płatności
            dokumentOptima.FormaPlatnosci = formaPlatnosci;
            if (!string.IsNullOrEmpty(_impTranDTO.RachunekAkronim))
            {
                try
                {
                    dokumentOptima.FormaPlatnosci.RachunekAkronim = _impTranDTO.RachunekAkronim;
                }
                catch
                {
                    throw new InvalidOperationException("Błąd - Brak rachunku : " + _impTranDTO.RachunekAkronim);
                }
            }
            dokumentOptima.NumerObcy = _impTranDTO.NumerObcy;
            dokumentOptima.Uwagi = _impTranDTO.Opis;
            dokumentOptima.MagazynZrodlowy = DokumentyCommonTools.UstawMagazyn(sesja, _impTranDTO);
            DokumentPozycjeCommand.Pozycje_FS_PA(sesja, dokumentOptima, _impTranDTO);
            dokumentOptima.BlokadaPlatnosci = 1;
            dokumentOptima.PlatnoscDokumentu.BlokadaGeneracjiPlatnosciPrzyZapisie = 1;
            
            foreach (var element in 
                dokumentOptima.Atrybuty)
            {
              var x =   element.GetType();
            }

            DokumentyCommonTools.UstawDateDokumentu(dokumentOptima, _impTranDTO);
            CommonToolsCommand.ZapiszSesje(sesja, _impTranDTO);

            return dokumentOptima;
        }
    }
}
