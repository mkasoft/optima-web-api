﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.asoft.OptimaLib.dtoOptima;
using com.asoft.OptimaLib.dtoService;
using OP_KASBOLib;

namespace OptimaSDKLib.Base
{

    public class PlatnosciToolsCommand
    {


        public static OP_KASBOLib.FormaPlatnosci ZnajdzFormePlatnosci(CDNBase.AdoSession Sesja, ImportDokumentDto tra)
        {
            String FormaPlat = "";
            String P_Forma_Platnosci = "";
            CDNBase.ICollection FormyPlatnosci = (CDNBase.ICollection)(Sesja.CreateObject("CDN.FormyPlatnosci", null));
            OP_KASBOLib.FormaPlatnosci FPl;


            if (tra.FormaPlatnosci != null && tra.FormaPlatnosci != 0)
            {
                P_Forma_Platnosci = "FPl_FPlId=" + tra.FormaPlatnosci + " and FPl_NieAktywny = 0";
                FPl = (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[P_Forma_Platnosci];
            }
            else
            {
                FormaPlat = "gotówka";
                P_Forma_Platnosci = "FPl_Nazwa=" + "'" + FormaPlat + "' and FPl_NieAktywny = 0";
                FPl = (OP_KASBOLib.FormaPlatnosci)FormyPlatnosci[P_Forma_Platnosci];



            }
            FPl.Termin = 0;
            if (tra.TerminPlatosci != null && tra.TerminPlatosci.Value.Year > 1)
            {
                TimeSpan ts;
                ts = ((DateTime)tra.TerminPlatosci).Subtract((DateTime)tra.DataDok);
                int ile_dni_termin = ts.Days;
                if (ile_dni_termin > 0)
                {
                    FPl.Termin = ile_dni_termin;
                }
            }

            return FPl;
        }

    }
}