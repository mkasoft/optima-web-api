﻿using System;
using System.Collections.Generic;
using System.Linq;
using CDNBase;
using CDNHlmn;
using com.asoft.OptimaLib.dtoService;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib
{
    public class DokumentPozycjeCommand
    {
        private const int DOKUMENT_BRUTTO = 2;
        private const int DOKUMENT_NETTO = 1;
        private const int KAUCJA_TAK = 1;

        public static void Pozycje_FZ(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag pDokumentOptima, ImportDokumentDto tra)
        {

            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);


            CDNBase.ICollection pozycje = pDokumentOptima.Elementy;
            List<ImportDokumentPozycjaDto> listaPozycjiDokumentuImportowanego = tra.PozycjeDokumentuImportowanego;
            foreach (ImportDokumentPozycjaDto tpz in listaPozycjiDokumentuImportowanego)
            {
                try
                {
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoFz(tra, tpz);
                    pozycja.TowarKod = tpz.TowarKod;
                    pozycja.Ilosc = (double)tpz.Ilosc;
                    pozycja.Cena0WD = (decimal)tpz.CenaNetto;
                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;

                }
                catch (Exception sql)
                {
                    CommonToolsCommand.ZalogujBlad("Klient:" + tra.KodDostawcy + "Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji ", nameof(DokumentPozycjeCommand), sql);
                    throw new System.InvalidOperationException("Klient:" + tra.KodDostawcy + "Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji ");
                }
            }


        }

        public static void Pozycje_MM(CDNBase.AdoSession Sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
          
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(Sesja, tra);


            CDNBase.ICollection Pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> ImportDokumentPozycjaDtoView = tra.PozycjeDokumentuImportowanego;
                string PozycjaDostawyLista = null;
                string atrybut10 = null;

                foreach (ImportDokumentPozycjaDto tpz in ImportDokumentPozycjaDtoView)
                {
                    CDNHlmn.IElementHaMag Pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew(null);

                    KodTwr = tpz.TowarKod;
                    Pozycja.TowarKod = tpz.TowarKod;
                    Pozycja.JM = tpz.JM;
                    Pozycja.Stawka = (decimal)tpz.Stawka;
                    Pozycja.Flaga = (int)tpz.Flaga;
                    Pozycja.Ilosc = (double)tpz.Ilosc;

                    if (P_DokumentOptima.TypNB == DOKUMENT_NETTO)
                    {
                        Pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                    }
                    else
                    {
                        Pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                    }

                    Pozycja.WywolajPrzeliczAgregaty();
                }
            }

            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, sql);
            }
         
        }

        public static void Pozycje_MMPlus(CDNBase.AdoSession Sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
       
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(Sesja, tra);


            CDNBase.ICollection Pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> ImportDokumentPozycjaDtoView = tra.PozycjeDokumentuImportowanego;
                Int32 IdDost;
                Boolean WybranoDostawe = false;
                foreach (ImportDokumentPozycjaDto tpz in ImportDokumentPozycjaDtoView)
                {                    
                    CDNHlmn.IElementHaMag Pozycja = (CDNHlmn.IElementHaMag)Pozycje["Tre_TreID=" + tpz.TpzIdCnt];
                    try
                    {
                        Pozycja.UstawDostawy();
                    }
                    catch (Exception e)
                    {
                        throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji. Towar: " + KodTwr + " Nie można dodać dostawy. Moduł HANDEL PLUS jest wyłączony. " + e.Message);
                    }


                    int NrDostawy = Pozycja.Dostawy.Count - 1;
                    WybranoDostawe = false;
                    for (int i = 0; i < Pozycja.Dostawy.Count; i++)
                    {
                        //tak nie działało - atrybut 10 ma wylko id dostawy i nie jest w systemie opisany
                        //Cecha10_Kod = Pozycja.Dostawy[i].Cecha10_Kod;

                        IdDost = Pozycja.Dostawy[i].TrsId;

                        if (tpz.DstId != null && IdDost == tpz.DstIdCnt)
                        {
                            Pozycja.Dostawy[i].Cecha10_Wartosc = tpz.DstId;
                            Pozycja.Dostawy[i].NumerPelny = tra.NumerPelny;
                            Pozycja.Dostawy[i].NumerObcy = tra.NumerObcy;
                            WybranoDostawe = true;
                        }


                    }
                    //Optima zmienia TrsId po zmnianie arybutu
                    // nie można zatem sprawdzać z dokładnością do dostawy
                    //przyjąłem - oby dobrze, że wystarczy znaleźć dobrą pozycję i z niej odczytać dostawy i zaznaczyć dst Id dla nich wszystkich 
                    //jeżeli nie udało się odszukać z dokładnością do doostawy
                    if (!WybranoDostawe)
                    {
                        for (int i = 0; i < Pozycja.Dostawy.Count; i++)
                        {

                            Pozycja.Dostawy[i].Cecha10_Wartosc = tpz.DstId;
                            Pozycja.Dostawy[i].NumerPelny = tra.NumerPelny;
                            Pozycja.Dostawy[i].NumerObcy = tra.NumerObcy;
                            WybranoDostawe = true;
                        }

                    }
                    if (!WybranoDostawe)
                    {
                        var exceptionWybranoDostawe = new System.InvalidOperationException(" Błąd Wstawiania pozycji. Brak dokumentu MMW w Optimie dstId:" + tpz.DstId );
                        CommonToolsCommand.ZalogujBlad(exceptionWybranoDostawe.Message, nameof(DokumentPozycjeCommand), exceptionWybranoDostawe);
                        throw exceptionWybranoDostawe;
                    }


                    Pozycja.WywolajPrzeliczAgregaty();


                }

            }
            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, sql);
            }


        }


        public static void Pozycje_PZ(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag pDokumentOptima, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);      
            CDNBase.ICollection pozycje = pDokumentOptima.Elementy;
            //Posortuj
            //TODO JKW może zmienić tyl na int w ImportDokumentPozycjaDto
            List<ImportDokumentPozycjaDto> ImportDokumentPozycjaDtoView = tra.PozycjeDokumentuImportowanego.OrderBy(o => o.Lp.Length).ThenBy(o => o.Lp).ToList();

            int pozycjaNaDokumentcie = 1;
            foreach (ImportDokumentPozycjaDto tpz in ImportDokumentPozycjaDtoView)
            {
                try
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoPz(tra, tpz);
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoPz(tra, tpz);
                    pozycja.Lp = pozycjaNaDokumentcie++;
                    pozycja.TowarKod = tpz.TowarKod;
                    
                    if (tpz.CenaNetto != null) pozycja.Cena0WD = (decimal)tpz.CenaNetto;

                    if (tpz.CenaNettoRabat != null) pozycja.CenaWWD = (decimal)tpz.CenaNettoRabat;
                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;
                    pozycja.Ilosc = (double)tpz.Ilosc;
                }
                catch (Exception sql)
                {
                    CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + tpz.TowarKod, nameof(DokumentPozycjeCommand), sql);
                    throw new System.InvalidOperationException(
                        "Błąd - wstawiania pozycji:" + tpz.TowarKod, sql);
                }

            }

        }



        public static void Pozycje_PKA(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag pDokumentOptima, ImportDokumentDto tra)
        {
           
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);


            CDNBase.ICollection pozycje = pDokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> listaPozycjiDokumentuZrodlowego = tra.PozycjeDokumentuImportowanego;

                foreach (ImportDokumentPozycjaDto tpz in listaPozycjiDokumentuZrodlowego)
                {
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoWkaPka(tra, tpz);
                    KodTwr = tpz.TowarKod;
                    pozycja.TowarKod = tpz.TowarKod;

                    //pozycja.Towar = 

                    pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                    pozycja.CenaWWD = (decimal)tpz.CenaBrutto;

                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;

                    //wersja bez wyboru z dostawy
                    pozycja.Ilosc = (double)tpz.Ilosc;

                }

            }
            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException("Błąd - wstawiania pozycji:" + KodTwr, sql);
            }

        }
        public static void Pozycje_PW(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);
            CDNBase.ICollection Pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> listaPozycjiDokumentuZrodlowego = tra.PozycjeDokumentuImportowanego;

                CDNTwrb1.Towary Towary = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);
                CDNTwrb1.ITowar Towar;

                foreach (ImportDokumentPozycjaDto tpz in listaPozycjiDokumentuZrodlowego)
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoRwOrPw(tra, tpz);
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew(null);

                    KodTwr = tpz.TowarKod;

                    pozycja.TowarKod = tpz.TowarKod;
                    Towar = (CDNTwrb1.ITowar)Towary["Twr_Kod='" + tpz.TowarKod + "'"];
                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;
                    pozycja.Ilosc = (double)tpz.Ilosc;

                    if (P_DokumentOptima.TypNB == DOKUMENT_NETTO)
                    {
                        pozycja.Cena0WD = (decimal)tpz.CenaNetto;
                        pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                        pozycja.WartoscNetto = (decimal)tpz.Netto;
                    }
                    else
                    {
                        pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                        pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                        pozycja.WartoscNetto = (decimal)tpz.Brutto;
                    }
                    pozycja.WywolajPrzeliczAgregaty();
                }

            }
            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr,sql);
            }


        }
        public static void Pozycje_RW(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);


            CDNBase.ICollection Pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> listaPozycjiDokumentuZrodlowego = tra.PozycjeDokumentuImportowanego;

                CDNTwrb1.Towary Towary = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);
                CDNTwrb1.ITowar Towar;

                foreach (ImportDokumentPozycjaDto tpz in listaPozycjiDokumentuZrodlowego)
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoRwOrPw(tra, tpz);
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew(null);

                    KodTwr = tpz.TowarKod;

                    pozycja.TowarKod = tpz.TowarKod;
                    Towar = (CDNTwrb1.ITowar)Towary["Twr_Kod='" + tpz.TowarKod + "'"];
                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;


                    if (tpz.CzyKaucja == KAUCJA_TAK || Towar.Kaucja == KAUCJA_TAK)
                    {
                        pozycja.Ilosc = (double)tpz.Ilosc;
                        pozycja.Cena0WD = (decimal)tpz.CenaNetto;
                        pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                        pozycja.WartoscNetto = (decimal)tpz.Netto;
                        continue;
                    }


                    pozycja.Ilosc = (double)tpz.Ilosc;

                    if (P_DokumentOptima.TypNB == DOKUMENT_NETTO)
                    {
                        pozycja.Cena0WD = (decimal)tpz.CenaNetto;
                        pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                        pozycja.WartoscNetto = (decimal)tpz.Netto;
                    }
                    else
                    {
                        pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                        pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                        pozycja.WartoscNetto = (decimal)tpz.Brutto;
                    }
                    pozycja.WywolajPrzeliczAgregaty();
                }

            }
            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr,sql);
            }

           
        }

        public static void Pozycje_WZ(AdoSession sesja, IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {

            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);

            CDNBase.ICollection pozycje = P_DokumentOptima.Elementy;
            List<ImportDokumentPozycjaDto> pozycjeImportowanejTransakcji =
                tra.PozycjeDokumentuImportowanego.OrderBy(o => o.Lp.Length).ThenBy(o => o.Lp).ToList();

            CDNTwrb1.Towary towary = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);
            foreach (ImportDokumentPozycjaDto tpz in pozycjeImportowanejTransakcji)
            {
                try
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoWz(tra, tpz);
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);
                    CDNTwrb1.ITowar towarOptima;
                    pozycja.TowarKod = tpz.TowarKod;
                    towarOptima = (CDNTwrb1.ITowar)towary["Twr_Kod='" + tpz.TowarKod + "'"];
                    pozycja.JM = tpz.JM;
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoWz(tra, tpz);
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;

                    if (tpz.CzyKaucja == KAUCJA_TAK || towarOptima.Kaucja == KAUCJA_TAK)
                    {
                        pozycja.Ilosc = (double)tpz.Ilosc;
                        pozycja.Cena0WD = (decimal)tpz.CenaNetto;
                        pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                        pozycja.WartoscNetto = (decimal)tpz.Netto;
                        continue;
                    }

                    pozycja.Ilosc = (double)tpz.Ilosc;

                    if (P_DokumentOptima.TypNB == DOKUMENT_NETTO)
                    {
                        pozycja.CenaWWD = (decimal)tpz.CenaNetto;
                    }
                    else
                    {
                        pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                    }


                    pozycja.WywolajPrzeliczAgregaty();
                }
                catch (Exception sql)
                {
                    CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + tpz.TowarKod, nameof(DokumentPozycjeCommand), sql);
                    throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + tpz.TowarKod , sql);
                }
            }

        }



        public static void Pozycje_WKA(CDNBase.AdoSession sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(sesja, tra);


            CDNBase.ICollection pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> listaPozycjiDokumentuZrodlowego = tra.PozycjeDokumentuImportowanego;

                foreach (ImportDokumentPozycjaDto tpz in listaPozycjiDokumentuZrodlowego)
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoWkaPka(tra, tpz);
                    CDNHlmn.IElementHaMag pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew(null);


                    pozycja.TowarKod = tpz.TowarKod;
                    pozycja.JM = tpz.JM;
                    pozycja.Stawka = (decimal)tpz.Stawka;
                    pozycja.Flaga = (int)tpz.Flaga;
                    pozycja.Ilosc = (double)tpz.Ilosc;
                    pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                    pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                }

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), e);
                throw new System.InvalidOperationException(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr,e);
            }
        }


        public static void Pozycje_FS_PA(CDNBase.AdoSession Sesja, CDNHlmn.IDokumentHaMag P_DokumentOptima, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            TowaryToolsCommand.TowarSprawdzNieDodawajJakNieMa(Sesja, tra);
            CDNBase.ICollection Pozycje = P_DokumentOptima.Elementy;
            String KodTwr = "";
            try
            {
                List<ImportDokumentPozycjaDto> ImportDokumentPozycjaDtoView = tra.PozycjeDokumentuImportowanego;

                foreach (ImportDokumentPozycjaDto tpz in ImportDokumentPozycjaDtoView)
                {
                    DokumentPozycjeWalidatorCommand.SprawdzPozycjeDokumentuZrodlowegoFz(tra, tpz);
                    CDNHlmn.IElementHaMag Pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew(null);
                    KodTwr = tpz.TowarKod;
                    Pozycja.TowarKod = tpz.TowarKod;
                    Pozycja.Ilosc = (double)tpz.Ilosc;
                    if (tpz.CenaBrutto > 0)
                    {
                        Pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                    }
                    else
                    {
                        Pozycja.Cena0WD = (decimal)tpz.CenaBrutto;
                    }
                    Pozycja.CenaWWD = (decimal)tpz.CenaBrutto;
                    if (tpz.Brutto != null)
                    {
                        Pozycja.WartoscBrutto = (decimal)tpz.Brutto;
                    }

                    if (!tpz.JM.Equals(Pozycja.JM))
                        throw new System.InvalidOperationException("Nie udało się wstawić pozycji niezgodne jednostki miary");

                    Pozycja.JM = tpz.JM;
                    Pozycja.Stawka = (decimal)tpz.Stawka;
                    Pozycja.Flaga = (int)tpz.Flaga;

                }

            }
            catch (Exception sql)
            {
                CommonToolsCommand.ZalogujBlad(" Dokument:" + tra.NumerPelny + " Błąd Wstawiania pozycji " + KodTwr, nameof(DokumentPozycjeCommand), sql);
                throw new System.InvalidOperationException("Klient:" + tra.KodDostawcy + "Dokument" + tra.NumerPelny + "Błąd Wstawiania pozycji " + KodTwr, sql);
            }


        }

        public static void Zmien_Pozycje(CDNHlmn.IDokumentHaMag pDokument, ImportDokumentDto tra)
        {
            CzySaPozycjeNaDokumencie(tra);
            CDNBase.ICollection pozycje = pDokument.Elementy;

            List<int> delete = new List<int>();
            String kodTwr = "";
            try
            {
                foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
                {
                    foreach (CDNHlmn.IElementHaMag Pozycja in pozycje)
                    {

                        if (Pozycja.Lp.Equals((int)tpz.NrPozPa))
                        {
                            kodTwr = tpz.TowarKod;
                            Pozycja.Ilosc = (double)tpz.Ilosc;
                            if (!tpz.JM.Equals(Pozycja.JM))
                            {

                               var exceptionJednostkaMiary = new System.InvalidOperationException("Nie udało się wstawić pozycji niezgodne jednostki miary");
                                CommonToolsCommand.ZalogujBlad(exceptionJednostkaMiary.Message, nameof(DokumentPozycjeCommand), exceptionJednostkaMiary);
                                throw exceptionJednostkaMiary;
                            }
                        }
                    }
                }
                int i = 0;

                foreach (CDNHlmn.IElementHaMag pozycja in pozycje)
                {
                    var jestNaKorekcie = false;
                    foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
                    {
                        if (pozycja.Lp.Equals((int)tpz.NrPozPa))
                        {
                            jestNaKorekcie = true;
                        }
                    }
                    i = pozycja.Lp;
                    if (!jestNaKorekcie)
                    {
                        delete.Add(i);
                    }
                }

                List<int> deleteRev = new List<int>();
                for (int ii = delete.Count; ii > 0; ii--)
                {
                    deleteRev.Add(delete[ii - 1]);
                }

                foreach (int del in deleteRev)
                {
                    pozycje.Delete(del - 1);
                }

                foreach (CDNHlmn.IElementHaMag pozycja in pozycje)
                {
                    pozycja.WywolajPrzeliczAgregaty();
                }

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("Dokument: " + tra.NumerPelny + ": Błąd Wstawiania pozycji " + kodTwr, nameof(DokumentPozycjeCommand), e);
                throw new System.InvalidOperationException("Dokument: " + tra.NumerPelny + ": Błąd Wstawiania pozycji " + kodTwr,e);

            }
          
        }
      

        public static void Zmien_Pozycje_KorektaCeny(CDNBase.AdoSession Sesja, CDNHlmn.IDokumentHaMag P_Dokument, ImportDokumentDto tra)
        {
            if (tra.PozycjeDokumentuImportowanego == null)
            {
                throw new System.InvalidOperationException(" Dokument: " + tra.NumerPelny + " Brak pozycji na dokumencie");
            }
            CDNBase.ICollection pozycje = P_Dokument.Elementy;
            List<int> delete = new List<int>();
            String kodTwr = "";
            try
            {
                foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
                {
                    foreach (CDNHlmn.IElementHaMag Pozycja in pozycje)
                    {
                        //w NrPozPa jest nr pozycji korygowanej
                        if (Pozycja.Lp.Equals((int)tpz.NrPozPa))
                        {
                            kodTwr = tpz.TowarKod;

                            if (P_Dokument.TypNB == DOKUMENT_NETTO)
                            {
                                Pozycja.WartoscNetto = -(decimal)tpz.WartoscKorektaCeny;
                            }
                            else
                            {
                                Pozycja.WartoscNetto = -(decimal)tpz.WartoscKorektaCeny;
                            }

                            if (!tpz.JM.Equals(Pozycja.JM))
                                throw new System.InvalidOperationException("Nie udało się wstawić pozycji niezgodne jednostki miary");
                        }
                    }
                }

                int i = 0;
                foreach (CDNHlmn.IElementHaMag pozycja in pozycje)
                {
                    var jestNaKorekcie = false;
                    foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
                    {//w NrPozPa jest nr pozycji korygowanej
                        if (pozycja.Lp.Equals((int)tpz.NrPozPa))
                        {
                            jestNaKorekcie = true;
                        }
                    }
                    i = pozycja.Lp;
                    if (!jestNaKorekcie)
                    {
                        delete.Add(i);
                    }
                }

                List<int> deleteRev = new List<int>();
                for (int ii = delete.Count; ii > 0; ii--)
                {
                    deleteRev.Add(delete[ii - 1]);
                }

                foreach (int del in deleteRev)
                {
                    pozycje.Delete(del - 1);
                }
                foreach (CDNHlmn.IElementHaMag pozycja in pozycje)
                {
                    pozycja.WywolajPrzeliczAgregaty();
                }

            }
            catch (Exception e)
            {
                throw new System.InvalidOperationException("Dokument" + tra.NumerPelny + ". Błąd Wstawiania pozycji " + kodTwr + e.Message);
            }        

        }

        private static void CzySaPozycjeNaDokumencie(ImportDokumentDto tra)
        {
            if (tra.PozycjeDokumentuImportowanego == null || tra.PozycjeDokumentuImportowanego.Count == 0)
            {
                throw new System.InvalidOperationException("Klient: " + tra.KodDostawcy + " Dokument: " + tra.NumerPelny + " Brak pozycji na dokumencie");
            }
        }
    }
    
}