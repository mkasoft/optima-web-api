﻿using System.Collections.Generic;
using System.Linq;


namespace com.asoft.OptimaLib.dtoOptima
{
    public class ImportowanyTypDokumentu
    {

        public static string FakturaZakupu = "FZ";
        public static string FakturaSprzedazy = "FS";
        public static string PrzyjecieWewnetrzne = "PW";
        public static string Paragon = "PA";
        public static string RozchodWewnetrzny = "RW";
        public static string WydanieZewnetrzne = "WZ";
        public static string PrzyjecieZewnetrzne = "PZ";
        public static string PrzesumiecieMM = "MM";
        public static string PrzyjecieKaucji = "PKA";
        public static string WydanieKaucji = "WKA";
        public static string RezerwacjaOdbiorcy = "RO";

        public  static readonly List<string> ListaDokokumnetow = new List<string>()
        {
            FakturaZakupu,
            FakturaSprzedazy,
            PrzyjecieWewnetrzne,
            Paragon,
            RozchodWewnetrzny,
            WydanieZewnetrzne,
            PrzyjecieZewnetrzne,
            PrzesumiecieMM,
            PrzyjecieKaucji,
            WydanieKaucji,
            RezerwacjaOdbiorcy
        };
        public void JestObslugiwany(string kodDokumentu)
        {            
            var znalezionyKodDokumentu = ListaDokokumnetow.FirstOrDefault(stringToCheck => stringToCheck.Contains(kodDokumentu));
            if (znalezionyKodDokumentu == null)
            {
                throw new System.InvalidOperationException("nieobsługiwany typ dokumentu:"+ kodDokumentu);
            }
        }

    }

}
