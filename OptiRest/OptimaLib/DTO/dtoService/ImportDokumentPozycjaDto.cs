﻿using System;

namespace com.asoft.OptimaLib.dtoService
{
    public class ImportDokumentPozycjaDto
    {
        public System.String Lp { get; set; }
        public string TowarKod { get; set; }
        public string TowarNazwa { get; set; }
        public Nullable<System.Decimal> Ilosc { get; set; }
        public Nullable<System.Decimal> CenaNetto { get; set; }
        public Nullable<System.Decimal> CenaNettoRabat { get; set; }
        public decimal? CenaBrutto { get; set; }
        public string JM { get;  set; }
        public Nullable<System.Decimal> Stawka { get; set; }
        public int Flaga { get;  set; }
  
        public Nullable<System.Decimal> Netto { get;  set; }   
        public Nullable<System.Decimal> Brutto { get;  set; }    
        public Nullable<System.Decimal> TpzIdCnt { get;  set; }
        public Nullable<System.Decimal> DstIdCnt { get;  set; }
        public Nullable<System.Decimal> DstId { get;  set; } 
        public int NrPozPa { get;  set; }
        public Nullable<System.Decimal> WartoscKorektaCeny { get; set; }
        public Nullable<System.Decimal> KosztWlasny { get; set; }
        public string NrPozDok { get; set; }
        public Nullable<System.Decimal> TwrIdTwrCnt { get; set; }        
   
        public int CzyKaucja { get; set; }

        /*
public override string ToString()
{
return "Dokument Pozycja" + " _lpPow=" + LpPow + " Lp= " + Lp +" "+ TowarKod +" "+ TowarNazwa + " "+ TowarNazwa 
+ " cena="+ Cena + " TraSElem= " + CntId  + " cena wg dostawy=" + CenaDost + " ilosc = "+ Ilosc
+ " _idDostOrderBy=" + IdDostOrderBy  ;
}
*/
    }
}