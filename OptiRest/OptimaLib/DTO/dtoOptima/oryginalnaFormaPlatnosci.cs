﻿using System;

namespace com.asoft.OptimaLib.dtoOptima
{
    public class OryginalnaFormaPlatnosci
    {
        public int Id;
        public  String RachunekAkronim;
        public OryginalnaFormaPlatnosci(int _id,String _RachunekAkronim)
        {
            this.Id = _id;
            this.RachunekAkronim = _RachunekAkronim;
        }
    }
}
