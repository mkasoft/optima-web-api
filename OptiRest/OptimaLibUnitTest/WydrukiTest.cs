﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaSDKLib.Base;
using OptimaSDKLib.Utils;
using System;
using com.asoft.OptimaLib.Service;
using System.Runtime.InteropServices;
using OP_DBIMPLib;
using com.asoft.OptimaLib.tools;
using com.asoft.OptimaLib.Base;

namespace OptimaLibUnitTest
{
    [TestClass]
    public class WydrukiTest
    {
        public WydrukiTest()
        {
            new com.asoft.OptimaLib.Service.OptimaSessionPool();
        }
     
        /*
        [TestMethod]
        public void EksportujOptimaFSJakoECODFa()
        {
            var sesja = com.asoft.OptimaLib.Service.OptimaSessionPool.GetSession();
            //TrN_TrNId dokumentu
            const int idFa = 220;
            //ścieżka do katalogu gdzie ma być umieszczony plik xml
            const string ecodDir = "c:\\temp\\";
            try
            {
                Console.WriteLine("##### eksportowanie dokumentu #####");
                var ecodService = (IServisAhold)sesja.CreateObject("DBImp.ServisAhold", null);
                ecodService.Login = sesja.Login;
                ecodService.MakeEcodFa(ecodDir, idFa);
                string query = "select top 1 TrN_NumerPelny from cdn.TraNag WHERE TrN_TrNId = " + idFa;
                var numerDok = GetValue.GetSingleValue(sesja, query, false);
                Console.WriteLine("##### wyeksportowano dokument: " + numerDok + " #####");
            }
            catch (COMException comError)
            {
                Console.WriteLine("###ERROR### Eksportowanie dokumentu nie powiodło się!\n{0}", ErrorInfo.Message(comError));
            }

        }

       */
        [TestMethod]
        public void PdfTest()
        {
            IWydrukiCommand wydrukiCommand = new WydrukiCommand();
            var sesja = OptimaSessionPool.GetSession();
            var plikWyeksportowny = wydrukiCommand.Pdf(sesja, 220);
            OptimaSessionPool.PutSession(sesja);
        }
    }
}
