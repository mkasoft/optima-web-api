﻿using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class PzController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;
        public PzController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }
        [HttpPost]
        public DokumentDto AddPz([FromBody]ImportDokumentDto impTranDto)
        {
            return _dokumentyService.DodajDokumentPz(impTranDto);
        }
    }
}