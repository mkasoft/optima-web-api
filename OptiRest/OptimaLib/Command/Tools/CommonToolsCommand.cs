﻿using System;
using CDNBase;
using com.asoft.OptimaLib.dtoService;
using log4net;
using LogManager = log4net.LogManager;

namespace OptimaSDKLib.Base
{
    public class CommonToolsCommand
    {
       
        public static Application Application = null;
        public static ILogin Login = null;
      
        public static AdoSession Sesja;
        public static void ZalogujBlad(string message, string loggerClassName, Exception e = null)
        {           
            ILog logger = LogManager.GetLogger(loggerClassName);            
            logger.Error(message, e);
        }

        public static void ZalogujWiadomosc(string message, string loggerClassName)
        {
            ILog logger = LogManager.GetLogger(loggerClassName);
            logger.Info(message);
        }

        public static void ZapiszSesje(AdoSession sesja, ImportDokumentDto tra)
        {           
            try
            {
                sesja.Save();               

            }
            catch (InvalidOperationException ex)
            {               
                ZalogujBlad("Błąd zapisu do systemu Comarch:" + ex.Message, nameof(CommonToolsCommand), ex);
                throw new InvalidOperationException("Błąd zapisu do systemu Comarch " + ex);

            }
            catch (Exception ex2)
            {                
                ZalogujBlad("Błąd zapisu do systemu Comarch:" + ex2.Message, nameof(CommonToolsCommand), ex2);
                throw new InvalidOperationException("Błąd zapisu do systemu Comarch: " + ex2);
            }
            ZalogujWiadomosc("Zapisałem dokument: " + tra.NumerPelny, nameof(CommonToolsCommand));
       }

        public static void ZapiszSesje(AdoSession sesja)
        {

            try
            {
                sesja.Save();
            }
            catch (InvalidOperationException ex)
            {
                ZalogujBlad("Błąd zapisu do systemu Comarch: " + ex.Message, nameof(CommonToolsCommand), ex);
                throw new InvalidOperationException("Błąd zapisu do systemu Comarch: " + ex);
            }
            catch (Exception ex2)
            {
                ZalogujBlad("Błąd zapisu do systemu Comarch: " + ex2.Message, nameof(CommonToolsCommand), ex2);
                throw new InvalidOperationException("Błąd zapisu do systemu Comarch: " + ex2);
            }
            ZalogujWiadomosc("Zapisano sesję do systemu Comarch", nameof(CommonToolsCommand));
        }
    }
}