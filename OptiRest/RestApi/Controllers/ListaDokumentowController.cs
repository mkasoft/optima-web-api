﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;

namespace RestApi.Controllers
{
    
    public class OperacjeNaDokumentachController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;

        public OperacjeNaDokumentachController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }

        [Route("api/Anuluj")]
        [HttpPut]
        public DokumentDto AnulujDokumentOptimaPoNumerzePelnym([FromUri]string numerPelnyDokumentu)
        {
            return _dokumentyService.AnulujDokumentOptimaPoNumerzePelnym(numerPelnyDokumentu);
        }

        [Route("api/ListaDokumentow")]
        [HttpGet]
        public List<DokumentDto> ListaDokumentow([FromUri]DokumentyKryteriaDto dokumentyKryteriaDto)
        {
            return _dokumentyService.ListaDokumentowOptimaNieAnulowanych(dokumentyKryteriaDto);
        }


    }
}