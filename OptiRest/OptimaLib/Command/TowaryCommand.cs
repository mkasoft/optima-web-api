﻿using System;
using System.Collections.Generic;
using ADODB;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.helpers;

namespace OptimaSDKLib.Base
{

    public class TowaryCommand : ITowaryCommand
    {
        public TowaryCommand()
        {

        }


        public List<TowaryDto> ListaTowarow(CDNBase.AdoSession sesja, TowaryKryteriaDto towaryKryteriaDto)
        {

            String queryString = "";
            if (towaryKryteriaDto.OstatniaModyfikacja > DateTime.MinValue)
            {
                queryString += " and  Twr_TS_Mod >='" + DateConverter.convertDateTimeToFormatedStringWithoutTime(towaryKryteriaDto.OstatniaModyfikacja) + "'";
            }



            if (towaryKryteriaDto.KodTowaru != null)
            {
                queryString += " and Twr_Kod = " + towaryKryteriaDto.KodTowaru;
            }

            if (towaryKryteriaDto.NazwaTowaru != null)
            {
                queryString += " and Twr_Nazwa = " + towaryKryteriaDto.NazwaTowaru;
            }


            ADODB.Connection connection = sesja.Connection;
            ADODB.Recordset rRs = new ADODB.Recordset();

            string select = "select top 1000 Twr_TwrId as ID from cdn.Towary where 1=1  " + queryString;

            rRs.Open(select, connection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 1);

            List<TowaryDto> listaTowarowDto = new List<TowaryDto>();
            CDNTwrb1.Towary towaryOptima = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);           

            CommonToolsCommand.ZalogujWiadomosc("Wykonano zapytanie: " + select + " znaleziono = " + rRs.RecordCount + " rekordy", nameof(TowaryCommand));

            CDNTwrb1.Towar towarOptima = null;
            while (rRs.EOF == false)
            {
                towarOptima = (CDNTwrb1.Towar)towaryOptima["Twr_TwrId=" + rRs.Fields["ID"].Value];

                TowaryDto towarDto = new TowaryDto();
                towarDto.Id = towarOptima.ID;                
                towarDto.TowarKod = towarOptima.Kod;
                towarDto.TowarNazwa = towarOptima.Nazwa;
                towarDto.JM = towarOptima.JM;
                if (towarOptima.Stempel != null)
                {
                    towarDto.DataModyfikacji = towarOptima.Stempel.TSMod;
                }

                listaTowarowDto.Add(towarDto);
                rRs.MoveNext();
            }

            return listaTowarowDto;
        }
        public List<ZasobyDto> ListaZasobow(AdoSession sesja, ZasobyKryteriaDto zasobyKryteriaDto)
        {
            String queryString = "";
            if (zasobyKryteriaDto.Data > DateTime.MinValue)
            {
                queryString += " and  TwI_Data >='" + DateConverter.convertDateTimeToFormatedStringWithoutTime(zasobyKryteriaDto.Data) + "'";
            }


            if (zasobyKryteriaDto.KodTowaru != null)
            {
                queryString += " and Twr_Kod ='" + zasobyKryteriaDto.KodTowaru + "'";
            }

            if (zasobyKryteriaDto.NazwaTowaru != null)
            {
                queryString += " and Twr_Nazwa ='" + zasobyKryteriaDto.NazwaTowaru + "'";
            }

            if (zasobyKryteriaDto.Mag_Symbol != null)
            {
                queryString += " and Mag_Symbol ='" + zasobyKryteriaDto.Mag_Symbol + "'";
            }
            else
            {
                queryString += " and Mag_Symbol is null";
            }

        

            
        string sql = SqlZasoby();
            return PobierzZasoby(sesja, sql + queryString);
        }

        private  List<ZasobyDto> PobierzZasoby(AdoSession sesja, String sql)
        {
            List<ZasobyDto> listaZasobow = new List<ZasobyDto>();
            var cn = sesja.Connection;
            var rs = new Recordset();

            rs.Open(sql, cn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly, 1);
            while (!rs.EOF)
            {
                var zasobyDto = new ZasobyDto();
                zasobyDto.TwI_Braki = Convert.ToInt64(rs.Fields["TwI_Braki"].Value);
                zasobyDto.TwI_Data = (DateTime)rs.Fields["TwI_Data"].Value;
                zasobyDto.TwI_Ilosc = Convert.ToDecimal(rs.Fields["TwI_Ilosc"].Value);
                if(!Convert.IsDBNull(rs.Fields["TwI_MagId"].Value))
                {
                    zasobyDto.TwI_MagId = Convert.ToInt64(rs.Fields["TwI_MagId"].Value);
                }                
                zasobyDto.TwI_Rezerwacje = Convert.ToDecimal(rs.Fields["TwI_Rezerwacje"].Value);
                zasobyDto.TwI_TwIId = Convert.ToInt64(rs.Fields["TwI_TwIId"].Value);
                zasobyDto.TwI_Wartosc = Convert.ToDecimal(rs.Fields["TwI_Wartosc"].Value);
                zasobyDto.TwI_Zamowienia = Convert.ToDecimal(rs.Fields["TwI_Zamowienia"].Value);
                zasobyDto.Twr_JM = (string)rs.Fields["Twr_JM"].Value;
                zasobyDto.Twr_JMZ = (string)rs.Fields["Twr_JMZ"].Value;

                zasobyDto.Twr_Kategoria = (string)rs.Fields["Twr_Kategoria"].Value;

                if (!Convert.IsDBNull(rs.Fields["Twr_KatId"].Value))
                {
                    zasobyDto.Twr_KatId = Convert.ToInt64(rs.Fields["Twr_KatId"].Value);
                }
                if (!Convert.IsDBNull(rs.Fields["Twr_KatZakId"].Value))
                {
                    zasobyDto.Twr_KatZakId = Convert.ToInt64(rs.Fields["Twr_KatZakId"].Value);
                }
               
                zasobyDto.Twr_Kod = (string)rs.Fields["Twr_Kod"].Value;
                zasobyDto.Twr_Nazwa = (string)rs.Fields["Twr_Nazwa"].Value;
                zasobyDto.Twr_NumerKat = (string)(rs.Fields["Twr_NumerKat"].Value);

                zasobyDto.Twr_Opis = (string)rs.Fields["Twr_Opis"].Value;
                zasobyDto.Twr_Produkt = Convert.ToInt16(rs.Fields["Twr_Produkt"].Value);
                zasobyDto.Twr_TwCNumer = Convert.ToInt32(rs.Fields["Twr_TwCNumer"].Value);
                zasobyDto.Twr_TwrId = Convert.ToInt64(rs.Fields["Twr_TwrId"].Value);
                zasobyDto.Twr_Typ = Convert.ToInt16(rs.Fields["Twr_Typ"].Value);
                if (!Convert.IsDBNull(rs.Fields["Mag_Symbol"].Value))
                {
                    zasobyDto.Mag_Symbol = (string)rs.Fields["Mag_Symbol"].Value;
                }
                            


                rs.MoveNext();

                listaZasobow.Add(zasobyDto);
            }


            return listaZasobow;


        }

        private static string SqlZasoby()
        {
            String SQL = " Select"
                             + " A.Twr_TwrId,"
                             + " A.Twr_Typ,"
                             + " A.Twr_Produkt,"
                             + " A.Twr_Kod,"
                             + " A.Twr_NumerKat,"
                             + " A.Twr_Nazwa,"
                             + " A.Twr_KatId,"
                             + " A.Twr_Kategoria,"
                             + " A.Twr_KatZakId,"
                             + " A.Twr_Opis,"
                             + " A.Twr_JM,"
                             + " A.Twr_JMZ,"
                             + " A.Twr_TwCNumer,"
                             + " B.TwI_MagId,"
                             + " B.TwI_Data,"
                             + " B.TwI_Ilosc,"
                             + " B.TwI_Braki,"
                             + " B.TwI_Rezerwacje,"
                             + " B.TwI_Zamowienia,"
                             + " B.TwI_Wartosc,"
                             + " B.TwI_TwIId,"
                             + " C.Mag_Symbol"
                             + " From  CDN.Towary A "
                             + " Left Outer Join CDN.TwrIlosci B On A.Twr_TwrID = B.TwI_TwrID"
                             + " Left Outer Join CDN.Magazyny C ON B.TwI_MagId= C.Mag_MagId"
                             + " where B.TwI_Data = ("
                             + "     Select Top 1 H.TwI_Data From CDN.TwrIlosci H Where H.TwI_TwrId = A.Twr_TwrId"
                             + "     Order By H.TwI_Data Desc )";
        
            return SQL;
        }
    }
}
 