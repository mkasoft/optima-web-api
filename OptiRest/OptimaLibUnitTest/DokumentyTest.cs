﻿using System;
using System.Collections.Generic;
using com.asoft.OptimaLib;
using CDNBase;
using CDNHlmn;
using com.asoft.OptimaLib.IOTools.DTO;
using com.asoft.OptimaLib.tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;

namespace OptimaLibUnitTest
{
    [TestClass]
    public class DokumentyTest
    {
        private static KonfiguracjaOptimaDto _konfiguracjaOptimaTest = new KonfiguracjaOptimaDto();
        private static Dokumenty _dokument = new Dokumenty();
        [ClassInitialize()]
        public static void InitTestSuite(TestContext testContext)
        {
           
            _konfiguracjaOptimaTest.CzyZapisywacTowary = true;
            _konfiguracjaOptimaTest.WyborDostaw = false;
            _konfiguracjaOptimaTest.CzyZapisywacTowary = true;
            _konfiguracjaOptimaTest.UserOptima = "TEST";
            _konfiguracjaOptimaTest.HasloOptima = "test";
            _konfiguracjaOptimaTest.BazaOptima = "TEST";
            _dokument.KonfiguracjaOptima= _konfiguracjaOptimaTest;

        }

        [ClassCleanup()]
        public static void CleanupTestSuite()
        {
            //to do cleanUp
        }

        [TestMethod]
        public void PdfTestMik()
        {
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();
            /////-------------------------
            Type type = Type.GetTypeFromProgID("CDN.WydrFormat", true);
            dynamic Format = Activator.CreateInstance(type);
            Format.GenRapReportType = 2;
            Format.GenRapNewReportInitObjects = "FakturaSpr";
                    
            Format.ZrodloID = 0;
           // Format.WydrukID = 605;
            Format.ID = 1257; // ID Wydruku!
            Format.ID = 271; // ID Wydruku!
            Format.ID = 325; // ID Wydruku!
            Format.ID = 1258; // ID Wydruku!


            Format.FiltrSQL = "TrN_TrNID = 220";
            Format.Urzadzenie = 4; //1 Ekran, 4 plik
            Format.FormatPlikuDocelowego = 2; //PDF
            Format.PlikDocelowy = "c:\\temp\\Faktura o ID "+System.DateTime.Now.Millisecond + Format.ID + ".pdf";

            CDNLib.Dictionary ZmiennaDyn = (CDNLib.Dictionary)Activator.CreateInstance(Type.GetTypeFromProgID("CDNLib.Dictionary"));
            dynamic ZmiennaDyn1 = Activator.CreateInstance(Type.GetTypeFromProgID("CDN.ZmiennaDyn"));
            ZmiennaDyn1.Nazwa = "IDENTYFIKATOR";
            ZmiennaDyn1.Wartosc = "220"; //ID faktury

            ZmiennaDyn.Add(ZmiennaDyn1.Nazwa, ZmiennaDyn1);

            IList<object> zaznaczenia = null;
            
                //Wykreowanie i dodanie zaznaczeń                    
                zaznaczenia = new List<object>();
                zaznaczenia.Add(94); //Dodajemy identyfikatory wybranych rekordów, np. wartości TrN_TrNID
                zaznaczenia.Add(93);
            
             Format.Wykonaj(ZmiennaDyn);
           


        }

        enum TypObslugiZaznaczenWydrukow
        {
            Brak = 0,
            TabelaTymczasowa = 1,
            KlauzulaIn = 2
        }

        public enum TypCGXWydruku
        {
            Brak = 0,
            Crystal = 1,
            GenRap = 2,
            XML = 3
        }

        public enum TypUrzadzenia
        {
            Brak = 1,
            Domyslne = 1,
            Podglad = 2,
            PodgladZKopiami = 3,
            DrukarkaDomyslna = 4,
            DrukarkaDomyslnaIloscKopii = 5,
            DrukarkaInna = 6,
            DrukarkaInnaIloscKopii = 7,
            DrukarkaInnaOpcje = 8,
            Eksport = 9,
            EksportOpcje = 10,
            Mail = 11,
            MailNaAdres = 12,
            MailOpcje = 13
        }

        [TestMethod]
        public void ListaDokumentowOptimaNieAnulowanych()
        {
            //Do testów należy w bazie optimy założyć dokument RW : RW/10/2019 o 3 pozycjach

            var commonTools = new CommonTools();
            commonTools.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            DokumentPozycje.CzyZapisywacTowary = true;
            commonTools.LogowanieAutomatyczne();
            AdoSession sesja = commonTools.SprawdzLogin();


            DokumentyKryteriaDto dokumentyKryteriaDto = new DokumentyKryteriaDto();
            dokumentyKryteriaDto.NrDokumentu = "RW/10/2019";
            dokumentyKryteriaDto.DataPoczatek = DateTime.MinValue;
            dokumentyKryteriaDto.DataKoniec = DateTime.MaxValue;

            List<DokumentDto> searchByNrDok = _dokument.ListaDokumentowOptimaNieAnulowanych(sesja, dokumentyKryteriaDto);
            Assert.AreEqual(searchByNrDok.Count, 1);
            foreach (DokumentDto dok in searchByNrDok)
            {
                Assert.AreEqual(dok.Pozycje.Count, 3);
            }

        }


        [TestMethod]
        public void MM_Dodaj()
        {
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();

            DokumentPozycje.CzyZapisywacTowary = true;
           
            var traMM = CommonToolsForTest.UtworzTransakcje("MM", "126/MM/2017");            
            traMM.exportMagazynDocelowy = "MAG_DOCELOWY";
          
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR-PZ", 3);

            UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traMM.ImpTransPozSnav = new List<ImpTransPozDTO>();
            traMM.ImpTransPozSnav.Add(pozycja1);
            traMM.ImpTransPozSnav.Add(pozycja2);
            traMM.ImpTransPozSnav.Add(pozycja3);

            var traPz = CommonToolsForTest.UtworzTransakcje("PZ", "126/PZ-MM/2017");
            traPz.ImpTransPozSnav = new List<ImpTransPozDTO>();
            traPz.ImpTransPozSnav.Add(pozycja1);
            traPz.ImpTransPozSnav.Add(pozycja2);
            traPz.ImpTransPozSnav.Add(pozycja3);
            var dokumentPz = _dokument.PZ_Dodaj(sesja, traPz);
            dokumentPz.Bufor = 0;
            CommonTools.ZapiszSesje(sesja, traPz);

            var dokumentMM = _dokument.MM_Dodaj(sesja, traMM);

            var bruttoMM = dokumentMM.RazemBrutto;
            var nettoMM= dokumentMM.RazemNetto;

            IDokumentHaMag dokumentOptima = DokumentyTools.ZnajdzDokumentPoTrNiD(sesja, dokumentMM.ID);

            var bruttoOptima = dokumentOptima.RazemBrutto;
            var nettoOptima = dokumentOptima.RazemNetto;
     
            Assert.AreEqual(18M, dokumentOptima.RazemNetto);
            Assert.AreEqual(dokumentMM.NumerPelny, dokumentOptima.NumerPelny);
            dokumentMM.Bufor = 0;
            CommonTools.ZapiszSesje(sesja, traMM);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentMM);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
        }

        [TestMethod]
        public void WZ_Dodaj()
        {
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();

            DokumentPozycje.CzyZapisywacTowary = true;


            string opisWZ = "126/WZ/2017";
            var traWz = CommonToolsForTest.UtworzTransakcje("WZ", opisWZ);
            
            traWz.exportMagazynDocelowy = "MAGAZYN";
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR-PZ", 3);

            UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traWz.ImpTransPozSnav = new List<ImpTransPozDTO>();
            traWz.ImpTransPozSnav.Add(pozycja1);
            traWz.ImpTransPozSnav.Add(pozycja2);
            traWz.ImpTransPozSnav.Add(pozycja3);
           
            var traPz = CommonToolsForTest.UtworzTransakcje("PZ", "126/PZ-WZ/2017");
             traPz.ImpTransPozSnav = new List<ImpTransPozDTO>();
             traPz.ImpTransPozSnav.Add(pozycja1);
             traPz.ImpTransPozSnav.Add(pozycja2);
             traPz.ImpTransPozSnav.Add(pozycja3);
             var dokumentPz= _dokument.PZ_Dodaj(sesja, traPz);
             dokumentPz.Bufor = 0;
             CommonTools.ZapiszSesje(sesja, traPz);
             
            var dokumentWz = _dokument.WZ_Dodaj(sesja, traWz);
          
            IDokumentHaMag dokumentOptima = DokumentyTools.ZnajdzDokumentPoTrNiD(sesja, dokumentWz.ID);

            Assert.AreEqual(321M, dokumentOptima.RazemBrutto);
            Assert.AreEqual(dokumentWz.NumerPelny, dokumentOptima.NumerPelny);
            dokumentWz.Bufor = 0;
            CommonTools.ZapiszSesje(sesja,traWz);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentWz);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
        }

       
     

        [TestMethod]
        public void RW_Dodaj()
        {
            var commonTools = new CommonTools();
            commonTools.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            DokumentPozycje.CzyZapisywacTowary = true;
            commonTools.LogowanieAutomatyczne();
            AdoSession sesja = commonTools.SprawdzLogin();


            var traRW = CommonToolsForTest.UtworzTransakcje("RW", "126/RW/2017");

            traRW.exportMagazynDocelowy = "MAGAZYN";
            var pozycja1 = CommonToolsForTest.UtworzPozycje(1M, 1M, "TOWAR-PZ", 3);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(2M, 2M, "TOWAR-PZ", 3);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(3M, 3M, "TOWAR-PZ", 3);

            UstawPozycjeDlaWZiRW(pozycja1, pozycja2, pozycja3);

            traRW.ImpTransPozSnav = new List<ImpTransPozDTO>();
            traRW.ImpTransPozSnav.Add(pozycja1);
            traRW.ImpTransPozSnav.Add(pozycja2);
            traRW.ImpTransPozSnav.Add(pozycja3);


            var traPz = CommonToolsForTest.UtworzTransakcje("PZ", "126/PZ-RW/2017");
            traPz.ImpTransPozSnav = new List<ImpTransPozDTO>();
            traPz.ImpTransPozSnav.Add(pozycja1);
            traPz.ImpTransPozSnav.Add(pozycja2);
            traPz.ImpTransPozSnav.Add(pozycja3);
            var dokumentPz = _dokument.PZ_Dodaj(sesja, traPz);
            dokumentPz.Bufor = 0;
        
            CommonTools.ZapiszSesje(sesja, traPz);

            var dokumentRW = _dokument.RW_Dodaj(sesja, traRW);    

            

            var bruttoRW = dokumentRW.RazemBrutto;
            var nettoRW = dokumentRW.RazemNetto;
           
            IDokumentHaMag dokumentOptima = DokumentyTools.ZnajdzDokumentPoTrNiD(sesja, dokumentRW.ID);
            
            var bruttoOptima = dokumentOptima.RazemBrutto;
            var nettoOptima = dokumentOptima.RazemNetto;
           
            Assert.AreEqual(304000, dokumentOptima.Rodzaj);
            Assert.AreEqual("RW", dokumentOptima.TypDokumentuString);
            //testy ręczne działają - wartośc dokumentu jest 95
            //Assert.AreEqual(95M, brutto);
            Assert.AreEqual(dokumentRW.NumerPelny, dokumentOptima.NumerPelny);

            dokumentRW.Bufor = 0;
            CommonTools.ZapiszSesje(sesja, traRW);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentRW);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
        }

       

        [TestMethod]
        public void PZ_Dodaj()
        {
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("PZ", "126/PZ/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(1.23M, 100M, "TOWAR-PZ", 3);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(12.3M, 10M, "TOWAR-PZ", 3);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(123M, 1M, "TOWAR-PZ", 3);
            pozycja1.Lp = "1";
            pozycja2.Lp = "2";
            pozycja3.Lp = "3";
            
            pozycja1.DstId = 1;
            pozycja2.DstId = 2;
            pozycja3.DstId = 3;
           
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);

            DokumentPozycje.CzyZapisywacTowary = true;
            var dokumentPz = _dokument.PZ_Dodaj(sesja, tra);
            dokumentPz.Bufor = 0;
            CommonTools.ZapiszSesje(sesja, tra);

            IDokumentHaMag dokumentOptima = DokumentyTools.ZnajdzDokumentPoTrNiD(sesja, dokumentPz.ID);
            Assert.AreEqual(300, dokumentOptima.RazemBrutto);
            Assert.AreEqual(dokumentPz.NumerPelny, dokumentOptima.NumerPelny);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPz);
        }

        [TestMethod]

        public void PKA_Dodaj()
        {

            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("PKA","126/PKA/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "OPAKOWANIE2", 5);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "OPAKOWANIE2", 5);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "OPAKOWANIE2", 5);
            pozycja1.CenaBrutto = 1;
            pozycja2.CenaBrutto = 2;
            pozycja3.CenaBrutto = 3;
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);


            tra.CzyZapisBufor = 0;
            var dokumentPka = _dokument.PKA_Dodaj(sesja, tra);

            tra.CzyZapisBufor = 0;


            CommonTools.ZapiszSesje(sesja);
           // DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPka);


        }

        [TestMethod]

        public void WKA_Dodaj()
        {

            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("WKA", "126/WKA/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "OPAKOWANIE", 4);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "OPAKOWANIE", 4);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "OPAKOWANIE", 4);
            pozycja1.CenaBrutto = 1;
            pozycja2.CenaBrutto = 2;
            pozycja3.CenaBrutto = 3;
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);


            tra.CzyZapisBufor = 0;
            var dokumentWka = _dokument.WKA_Dodaj(sesja, tra);
            tra.CzyZapisBufor = 0;


            CommonTools.ZapiszSesje(sesja);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentWka);


        }

        [TestMethod]

        public void FZ_Dodaj()
        {

            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("FZ", "126/FZ/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR Test", 2);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR Test", 2);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR Test", 2);
            pozycja1.CenaNettoTrn = 1;
            pozycja2.CenaNettoTrn = 2;
            pozycja3.CenaNettoTrn = 3;
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);

          
            tra.CzyZapisBufor = 0;
            var dokumentFz = _dokument.FZ_Dodaj(sesja, tra);
            tra.CzyZapisBufor = 0;
           

            CommonTools.ZapiszSesje(sesja);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentFz);
          

        }

        [TestMethod]
        public void FS_DogenerujKorekte()
        {

            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("FA", "126/FS/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR Test", 2);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR Test", 2);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR Test", 2);
            //Określenie która pozycja będzie korygowana
            pozycja1.NrPozPa = 1;
            pozycja2.NrPozPa = 2;
            pozycja3.NrPozPa = 3;
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);

            tra.exportKod = "FA";
            tra.CzyZapisBufor = 0;
            var dokumentFs = _dokument.FS_PA_Dodaj(sesja, tra);
            dokumentFs.NumerObcy = dokumentFs.NumerPelny;
            var zatwierdzony = dokumentFs.Zatwierdzony;
            CommonTools.ZapiszSesje(sesja);
            tra.Opis = dokumentFs.NumerObcy;
            var korektaFs = _dokument.FS_PA_DogenerujKorekte(sesja, tra);

           // DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentFs);
            
        }
        [TestMethod]
        public void FS_PA_Dodaj()
        {
           
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("FA", "126/PA/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR Test", 2);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR Test", 2);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR Test", 2);
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);
            
            tra.exportKod = "FA";
            tra.CzyZapisBufor = 0;
            var dokumentFs = _dokument.FS_PA_Dodaj(sesja, tra);
            tra.CzyZapisBufor = 0;
            tra.exportKod = "PA";
            var dokumentPa = _dokument.FS_PA_Dodaj(sesja, tra);
            
            CommonTools.ZapiszSesje(sesja);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentFs);
            DokumentyAnuluj.AnulujDokumentOptima(sesja, dokumentPa);

        }
        [TestMethod]
        public void FS_PA_Dodaj_10()
        {
            int Ile_Dokumentow_Dodac = 10;
            var optimaUtliLib = new CommonTools();
            optimaUtliLib.KonfiguracjaOptimaDto = _konfiguracjaOptimaTest;
            optimaUtliLib.LogowanieAutomatyczne();
            AdoSession sesja = optimaUtliLib.SprawdzLogin();


            var tra = CommonToolsForTest.UtworzTransakcje("FA", "126/FA/2017");
            var pozycja1 = CommonToolsForTest.UtworzPozycje(11.22M, 1M, "TOWAR Test", 2);
            var pozycja2 = CommonToolsForTest.UtworzPozycje(22.22M, 2M, "TOWAR Test", 2);
            var pozycja3 = CommonToolsForTest.UtworzPozycje(33.22M, 3M, "TOWAR Test", 2);
            tra.ImpTransPozSnav = new List<ImpTransPozDTO>();
            tra.ImpTransPozSnav.Add(pozycja1);
            tra.ImpTransPozSnav.Add(pozycja2);
            tra.ImpTransPozSnav.Add(pozycja3);
            for (var i = 0; i < Ile_Dokumentow_Dodac; i++)
            {
                tra.exportKod = "PA";
                _dokument.FS_PA_Dodaj(sesja, tra);
                tra.exportKod = "PA";
                _dokument.FS_PA_Dodaj(sesja, tra);
            }
            CommonTools.ZapiszSesje(sesja);

        }

        private static void UstawPozycjeDlaWZiRW(ImpTransPozDTO pozycja1, ImpTransPozDTO pozycja2, ImpTransPozDTO pozycja3)
        {
            pozycja1.Lp = "1";
            pozycja2.Lp = "2";
            pozycja3.Lp = "3";

            pozycja1.DstId = 1;
            pozycja1.CenaNettoTrn = 1M;
            pozycja1.CenaSprzPln = 1M;
            pozycja1.KosztWlasny = 4M;
            pozycja1.Netto = 1M;
            pozycja1.Brutto = 1.23M;
            pozycja1.CenaNetto = 3M;

            pozycja2.DstId = 2;
            pozycja2.CenaSprzPln = 10M;
            pozycja2.KosztWlasny = 4M;
            pozycja2.CenaNettoTrn = 2M;
            pozycja2.Netto = 2M;
            pozycja2.Brutto = 2.46M;
            pozycja2.CenaNetto = 3M;

            pozycja3.DstId = 3;
            //WZ - cena
            pozycja3.CenaSprzPln = 100M;
            pozycja3.KosztWlasny = 4M;
            //RW - cena
            pozycja3.CenaNettoTrn = 30M;
            pozycja3.CenaNetto = 3M;
            pozycja3.Netto = 3M;
            pozycja3.Brutto = 3.69M;
        }


    }
}