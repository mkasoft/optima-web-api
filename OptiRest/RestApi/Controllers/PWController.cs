﻿using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class PwController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;
        public PwController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }
        [HttpPost]
        public DokumentDto AddPw([FromBody]ImportDokumentDto impTranDto)
        {
            //var path = HttpContext.Current.Request.MapPath("~/optimasrc");
            return _dokumentyService.DodajDokumentPw(impTranDto);
        }
    }
}