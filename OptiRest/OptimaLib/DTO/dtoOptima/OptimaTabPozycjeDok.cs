using System;

namespace com.asoft.OptimaLib.dtoOptima
{
    public class OptimaTabPozycjeDok
    {
        public String TrE_GrupaPod { get; set; }
        public decimal TrE_CenaPoRabacie { get; set; }
        public String TrE_TwrKod { get; set; }
        public decimal TrE_Ilosc { get; set; }
        public decimal TrE_Cena { get; set; }
        public decimal TrE_Pozycja { get; set; }
           
        public OptimaTabPozycjeDok() { }
        public OptimaTabPozycjeDok(decimal Pozycja, String GrupaPod, decimal CenaPoRabacie, String TwrKod, decimal Ilosc,decimal Cena)
        {
            this.TrE_GrupaPod = GrupaPod;
            this.TrE_CenaPoRabacie = CenaPoRabacie;
            this.TrE_TwrKod = TwrKod;
            this.TrE_Ilosc = Ilosc;
            this.TrE_Cena = Cena;
            this.TrE_Pozycja = Pozycja;
        }
                
    }
}