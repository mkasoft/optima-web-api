﻿using System;
using System.Collections.Generic;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using log4net;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib.Service
{
   
    public class DokumentyService : IDokumentyService
    {
        private readonly IDokumentCommand _dokumentCommand;


        public DokumentyService(IDokumentCommand dokumentCommand)
        {
            _dokumentCommand = dokumentCommand ?? throw new ArgumentNullException(nameof(dokumentCommand));
        }
        public DokumentDto AnulujDokumentOptimaPoNumerzePelnym(string numerPelnyDokumentu)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                return DokumentDtoFromDokumentOptima(_dokumentCommand.AnulujDokumentOptimaPoNumerzePelnym(sesja, numerPelnyDokumentu));

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd anulowania dokumentu)  - " + e.Message, nameof(DokumentyService), e);
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                throw new OptimaDokumentException(10009,"ERROR -  (Błąd anulowania dokumentu)", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        public List<DokumentDto> ListaDokumentowOptimaNieAnulowanych(DokumentyKryteriaDto dokumentyKryteriaDto)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();               
                var listaDokumentow = _dokumentCommand.ListaDokumentowOptimaNieAnulowanych(dokumentyKryteriaDto, sesja);
                return listaDokumentow;

            }
            catch (Exception e)
            {
               
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd wyszukiwania listy dokumentow) - " + e.Message, nameof(DokumentyService), e);
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                throw new OptimaDokumentException(10000, "ERROR -  (Błąd wyszukiwania listy dokumentow)", dokumentyKryteriaDto, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        public DokumentDto DodajDokumentWz(ImportDokumentDto impTranDTO)
        {
            
            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.WZ_Dodaj(impTranDTO, sesja);               
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy); 

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu WZ)" + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10008, "ERROR -  (Błąd dodawania dokumentu WZ)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        public DokumentDto DodajDokumentPz(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.PZ_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu PZ) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10003, "ERROR -  (Błąd dodawania dokumentu PZ)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        public DokumentDto DodajDokumentFz(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.FZ_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu Faktura Zakupu FZ) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10005, "ERROR -  (Błąd dodawania dokumentu FZ)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        public DokumentDto DodajDokumentFs(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.FaktureSprzedazy_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu Faktury Sprzedaży) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10004, "ERROR -  (Błąd dodawania dokumentu Faktury Sprzedaży)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        public DokumentDto DodajDokumentPa(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.Paragon_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania Paragonu) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10006, "ERROR -  (Błąd dodawania Paragonu)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        public DokumentDto DodajDokumentRo(ImportDokumentDto impTranDTO)
        {
           
            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.Rezerwacje_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania Paragonu) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaDokumentException(10006, "ERROR -  (Błąd dodawania Paragonu)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

        public DokumentDto DodajDokumentRw(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.RW_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu RW) - " + e.Message, nameof(DokumentyService), e);
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                throw new OptimaDokumentException(10007, "ERROR -  (Błąd dodawania dokumentu RW)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        public DokumentDto DodajDokumentPw(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                
                var dodanyDokumentOptimy = _dokumentCommand.PW_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu PW) - " + e.Message, nameof(DokumentyService), e);
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                throw new OptimaDokumentException(10002, "ERROR -  (Błąd dodawania dokumentu PW)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        public DokumentDto DodajDokumentMm(ImportDokumentDto impTranDTO)
        {

            AdoSession sesja = null;
            try
            {
                sesja = OptimaSessionPool.GetSession();
                var dodanyDokumentOptimy = _dokumentCommand.MM_Dodaj(impTranDTO, sesja);
                return DokumentDtoFromDokumentOptima(dodanyDokumentOptimy);

            }
            catch (Exception e)
            {
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd dodawania dokumentu MM) - " + e.Message, nameof(DokumentyService), e);
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                throw new OptimaDokumentException(10002, "ERROR -  (Błąd dodawania dokumentu MM)", impTranDTO, e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }
        

        private static DokumentDto DokumentDtoFromDokumentOptima(CDNHlmn.IDokumentHaMag dodanyDokumentOptimy)
        {
            DokumentDto dokumentDto = new DokumentDto();

            dokumentDto.Id = dodanyDokumentOptimy.ID;
            dokumentDto.Anulowany = dodanyDokumentOptimy.Anulowany;
            dokumentDto.Brutto = dodanyDokumentOptimy.RazemBrutto;
            dokumentDto.DataWyst = dodanyDokumentOptimy.DataWys;
            dokumentDto.MagazynZrodlowy = dodanyDokumentOptimy.MagazynZrdNazwa;
            dokumentDto.Netto = dodanyDokumentOptimy.RazemNetto;
            dokumentDto.NumerPelny = dodanyDokumentOptimy.NumerPelny;
            dokumentDto.NumerPelnyKorekty = dodanyDokumentOptimy.KorektaZTytulu;
            if (dodanyDokumentOptimy.Podmiot != null)
            {
                dokumentDto.PodmiotAkronim = dodanyDokumentOptimy.Podmiot.Akronim;
            }            
            dokumentDto.Rodzaj = dodanyDokumentOptimy.Rodzaj;
            dokumentDto.Vat = dodanyDokumentOptimy.RazemVAT;
            //dokumentDto.Pozycje  nic na zapas!
            return dokumentDto;
        }


    }
}
