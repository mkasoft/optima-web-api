﻿using System;
using System.Collections.Generic;
using ADODB;
using CDNBase;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;

namespace com.asoft.OptimaLib.Service
{
    public interface IDokumentyService
    {

        List<DokumentDto> ListaDokumentowOptimaNieAnulowanych(DokumentyKryteriaDto dokumentyKryteriaDto);
        DokumentDto AnulujDokumentOptimaPoNumerzePelnym(string nrDokumentu);
        
        DokumentDto DodajDokumentWz(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentPz(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentFs(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentFz(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentPw(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentRw(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentMm(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentPa(ImportDokumentDto impTranDTO);
        DokumentDto DodajDokumentRo(ImportDokumentDto impTranDTO);

    }
}