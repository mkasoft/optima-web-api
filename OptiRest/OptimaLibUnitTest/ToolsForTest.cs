﻿using com.asoft.OptimaLib.dtoService;

namespace OptimaLibUnitTest
{
    public class ToolsForTest
    {

        public static ImportDokumentDto UtworzTransakcje(string rodzajDokumentu, string opis)
        {
            ImportDokumentDto tra = new ImportDokumentDto();
            tra.CzyZapisBufor = 1;
            tra.FormaPlatnosci = 1;           
            // tra.NrDok = 126M;
            tra.MagazynZrodlowy = "MAGAZYN";
            tra.Opis = opis;
            tra.NumerPelny = opis;
            tra.ExportKod = rodzajDokumentu;
            return tra;
        }

        public static ImportDokumentPozycjaDto UtworzPozycje(decimal pozycjaCenaBrutto, decimal pozycjaIloscJmTwr, string pozycjaNazwaTwr, int pozycjaTwrIdTwrCnt)
        {
            ImportDokumentPozycjaDto pozycja = new ImportDokumentPozycjaDto();
            pozycja.JM = "szt";
            pozycja.Stawka = 0;
            pozycja.Flaga = 2;
           
            pozycja.CenaBrutto = pozycjaCenaBrutto;
            pozycja.Brutto = System.Math.Round(pozycjaCenaBrutto * pozycjaIloscJmTwr,2);
            pozycja.CenaNetto = System.Math.Round(pozycjaCenaBrutto / 1.23M,2);
            //pozycja.IloscJmTwr = pozycjaIloscJmTwr;
            pozycja.Ilosc = pozycjaIloscJmTwr;
            pozycja.TwrIdTwrCnt = pozycjaTwrIdTwrCnt;           
            pozycja.TowarNazwa = pozycjaNazwaTwr;
            pozycja.TowarKod = pozycjaNazwaTwr;            
            return pozycja;
        }

        public static void UstawPozycjeDlaWZiRW(ImportDokumentPozycjaDto pozycja1, ImportDokumentPozycjaDto pozycja2, ImportDokumentPozycjaDto pozycja3)
        {
            pozycja1.Lp = "1";
            pozycja2.Lp = "2";
            pozycja3.Lp = "3";

            pozycja1.DstId = 1;
            pozycja1.CenaNetto = 1M;
            pozycja1.CenaBrutto = 1M;
            pozycja1.KosztWlasny = 4M;
            pozycja1.Netto = 1M;
            pozycja1.Brutto = 1.23M;
            pozycja1.CenaNetto = 3M;

            pozycja2.DstId = 2;
            pozycja2.CenaBrutto = 10M;
            pozycja2.KosztWlasny = 4M;
            pozycja2.CenaNetto = 2M;
            pozycja2.Netto = 2M;
            pozycja2.Brutto = 2.46M;
            pozycja2.CenaNetto = 3M;

            pozycja3.DstId = 3;
            //WZ - cena
            pozycja3.CenaBrutto = 100M;
            pozycja3.KosztWlasny = 4M;
            //RW - cena
            pozycja3.CenaNetto = 30M;
            pozycja3.CenaNetto = 3M;
            pozycja3.Netto = 3M;
            pozycja3.Brutto = 3.69M;
        }
    }
}