﻿using System;

namespace com.asoft.OptimaLib.dtoService
{
    public class DokumentPozycjaDto
    {
        public override string ToString()
        {
            return "Dokument Pozycja" + " Id= " + Id + " Lp= " + Lp + " TowarKod = " + TowarKod +" TowarNazwa = "+ TowarNazwa 
                 + " cena="+ Cena + " cena wg dostawy=" + CenaDost + " ilosc = "+ Ilosc ;
        }

        public long Lp { get; set; }

        public long Id { get; set; }
        public int MagID { get; set; }
        public long TowarID { get; set; }
        public string TowarKod { get; set; }
        public string TowarNazwa { get; set; }
        public decimal Cena { get; set; }
        public decimal Wartosc { get; set; }
        public double Ilosc { get; set; }
        public decimal WartoscZakupu { get; set; }
        public decimal CenaDost { get; set; }       
        public string JM { get; set; }
       
     
    }
}