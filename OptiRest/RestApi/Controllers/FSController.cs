﻿using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class FsController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;
        public FsController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }
        [HttpPost]
        public DokumentDto AddFs([FromBody]ImportDokumentDto impTranDto)
        {
            return _dokumentyService.DodajDokumentFs(impTranDto);

        }
    }
}