﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ADODB;
using CDNBase;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib.Service
{
    public class TowaryService : ITowaryService
    {
        private readonly ITowaryCommand _towaryCommand;
        public TowaryService(ITowaryCommand towaryCommand)
        {
            _towaryCommand = towaryCommand ?? throw new ArgumentNullException(nameof(towaryCommand));
        }

        List<TowaryDto> ITowaryService.ListaTowarow(TowaryKryteriaDto towaryKryteriaDto)
        {
            AdoSession sesja = null;
            string sql = "";
            try
            {
                sesja = OptimaSessionPool.GetSession();

                return _towaryCommand.ListaTowarow(sesja, towaryKryteriaDto);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd czytania towarów z Optimy) - " + e.Message, nameof(TowaryService), e);
                throw new OptimaTowaryException(30000,towaryKryteriaDto, "ERROR -  (Błąd czytania towarów z Optimy)", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }


        List<ZasobyDto> ITowaryService.ListaZasobow(ZasobyKryteriaDto zasobyKryteriaDto)
        {
            AdoSession sesja = null;
            string sql = "";
            try
            {
                sesja = OptimaSessionPool.GetSession();

                return _towaryCommand.ListaZasobow(sesja, zasobyKryteriaDto);

            }
            catch (Exception e)
            {
                sesja = OptimaSessionPool.RecreateNewSession(sesja);
                CommonToolsCommand.ZalogujBlad("ERROR -  (Błąd czytania zasobów z Optimy) - " + e.Message, nameof(DokumentyService), e);
                throw new OptimaTowaryException(30001, zasobyKryteriaDto, "ERROR -  (Błąd czytania zasobów z Optimy)", e);
            }
            finally
            {
                if (sesja != null)
                {
                    OptimaSessionPool.PutSession(sesja);
                }
            }
        }

    }
}