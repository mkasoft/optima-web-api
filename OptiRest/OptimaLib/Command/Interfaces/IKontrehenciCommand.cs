﻿using CDNBase;
using CDNHlmn;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Base
{
    public interface IKontrahenciCommand
    {
        List<KontrahentDto> ListaKontrahentow(AdoSession sesja);
        List<KontrahentDto> ListaKontrahentow(AdoSession sesja, String dataOstatniejModyfikacji);
    }
}
