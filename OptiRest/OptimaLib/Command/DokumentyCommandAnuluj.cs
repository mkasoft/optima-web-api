﻿using System;
using CDNBase;
using CDNHlmn;
using com.asoft.Base;
using OptimaSDKLib.Base;

namespace com.asoft.OptimaLib
{
    public class DokumentyCommandAnuluj
    {


        public static IDokumentHaMag AnulujDokumentOptimaPoNumerzePelnym(AdoSession sesja, string nrDokumentu)
        {     
            CDNHlmn.IDokumentHaMag dokumentOptima;
            try
            {
                dokumentOptima = DokumentyCommonTools.ZnajdzDokumentOptimaNieAnulowanyPoNumerzePelnym(sesja, nrDokumentu);
                AnulujDokumentOptima(sesja, dokumentOptima);
            }
            catch (Exception ex)
            {
                CommonToolsCommand.ZalogujBlad("Błąd: Nie udało się anulować dokumentu w Optimie " + nrDokumentu, nameof(DokumentyCommandAnuluj), ex);
                throw new System.InvalidOperationException("Błąd: Nie udało się anulować dokumentu w Optimie " + nrDokumentu + " ",ex);
            }
            return dokumentOptima;
        }
        
        public static void AnulujDokumentOptima(AdoSession sesja, IDokumentHaMag dokumentOptima)
        {
            if (dokumentOptima.Bufor == 1)
            {
                
                var exceptionBufor =  new System.InvalidOperationException("Błąd: Dokument jest w buforze nie można go anulować" + dokumentOptima.NumerPelny);
                CommonToolsCommand.ZalogujBlad(exceptionBufor.Message, nameof(DokumentyCommandAnuluj), exceptionBufor);
                throw exceptionBufor;
            }    
            if (dokumentOptima.CzyMoznaAnulowac() != 0)
            {
                dokumentOptima.AnulujDok(dokumentOptima.ID);
            }

            else
            {               
                var exceptionCechyDokumentu = new System.InvalidOperationException("Błąd: Cechy dokumentu uniemożliwiają anulowanie. " + dokumentOptima.NumerPelny);
                CommonToolsCommand.ZalogujBlad(exceptionCechyDokumentu.Message, nameof(DokumentyCommandAnuluj), exceptionCechyDokumentu);
                throw exceptionCechyDokumentu;
            }
            CommonToolsCommand.ZapiszSesje(sesja);
        }
        
    }
}