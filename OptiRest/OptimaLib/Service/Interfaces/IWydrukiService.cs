﻿using System;
using System.Collections.Generic;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;

namespace com.asoft.OptimaLib.Service
{
    public interface IWydrukiService
    {

        string pdf(long TrnId);
    }
}