﻿using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.asoft.OptimaLib.Service
{
    public class OptimaKontrahentException : Exception
    {
        public int ErrorNumber { get; set; }
        public string sql { get; set; }
      

        public OptimaKontrahentException()
        {
        }

        public OptimaKontrahentException(string message)
            : base(message)
        {
        }


        public OptimaKontrahentException(int _errorNumber,string _sql, string message, Exception inner)
     : base(message , inner)
        {
            sql = _sql;
            ErrorNumber = _errorNumber;        }
        

        public OptimaKontrahentException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public string GetPayloadError()
        {
            if (sql != null)
            {
                return sql;
            }           

            return "";
        }
    }
}


