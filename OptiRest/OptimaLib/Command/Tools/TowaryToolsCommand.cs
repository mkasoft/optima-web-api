﻿using System;
using System.Collections.Generic;
using com.asoft.OptimaLib.Base;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.helpers;

namespace OptimaSDKLib.Base
{

    public class TowaryToolsCommand
    {


        public static bool TowarSprawdzDodajJakNieMa(CDNBase.AdoSession sesja, ImportDokumentDto tra)
        {
            //funkcja sprawdz czy w optimie są towary
            //jeżeli ich brak to dodaje
            //sprawdzanie jest po kodzie i Id (zdjąć odpowiedni komentrza odszukujący towar
            CDNTwrb1.Towary cdnListaTowarow = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);
            foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
            {
                CDNTwrb1.ITowar cdnTowar;          
                try
                    {
                        //Sprawdź czy pod tym kodem już nie ma wpisu i dodawaj wyłącznie jeśli nie znajdziesz
                        cdnTowar = (CDNTwrb1.ITowar)cdnListaTowarow["Twr_Kod='" + tpz.TowarKod + "'"];                       
                }
                    catch
                    {
                        cdnTowar = (CDNTwrb1.ITowar)cdnListaTowarow.AddNew(null);

                        cdnTowar.Nazwa = tpz.TowarNazwa;
                       
                        cdnTowar.Kod = tpz.TowarKod;

                        cdnTowar.Stawka = (decimal)tpz.Stawka;
                        cdnTowar.Flaga = (short)tpz.Flaga;

                        cdnTowar.JM = tpz.JM;
                        sesja.Save();

                        cdnTowar = (CDNTwrb1.ITowar)cdnListaTowarow["Twr_Kod='" + tpz.TowarKod + "'"];
                    }
               
            }


            return true;
        }

        public static void TowarSprawdzNieDodawajJakNieMa(CDNBase.AdoSession sesja, ImportDokumentDto tra)
        {

            //funkcja sprawdz czy w optimie są towary
            //sprawdzanie jest po kodzie i Id (zdjąć odpowiedni komentrza odszukujący towar
            CDNTwrb1.Towary cdnListaTowarow = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary", null);
            foreach (ImportDokumentPozycjaDto tpz in tra.PozycjeDokumentuImportowanego)
            {
                CDNTwrb1.ITowar cdnTowar;

                try
                {
                    cdnTowar = (CDNTwrb1.ITowar)cdnListaTowarow["Twr_Kod='" + tpz.TowarKod + "'"];
                    //cdnTowar = (CDNTwrb1.ITowar) cdnListaTowarow["Twr_TwrId=" + tpz.TwrIdTwrCnt];
                    //Towar = (CDNTwrb1.ITowar)Towary["Twr_Kod='" + tpz.TowarKod + "'"];
                    // tra.optimaErrorMessage += "Jest towar: " + tpz.TwrIdTwrCnt + " " + tpz.TowarKod + " " + tpz.TowarNazwa + Environment.NewLine; 
                }
                catch
                {
                    var exception = new System.InvalidOperationException("Występują niezdefniowane towary w Optimie:" +
                                                               tpz.NrPozDok + " id=" + tpz.TwrIdTwrCnt + " " +
                                                               tpz.TowarKod + " " + tpz.TowarNazwa);

                    CommonToolsCommand.ZalogujBlad(exception.Message, nameof(TowaryToolsCommand), exception);
                    throw exception;
                }

            }

        }

    }
}