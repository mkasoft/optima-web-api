﻿using Microsoft.Win32;
using System.Collections;

namespace OptimaSDKLib.Utils
{
    #region Registry enums

    public enum RegutilKeys
    {
        HKCR = 0,
        HKCU = 1,
        HKLM = 2,
        HKU = 3,
        HKCC = 4
    }

    #endregion

    public class RegUtil
    {
        public const int REG_ERR_NULL_REFERENCE = 12;
        public const int REG_ERR_SECURITY = 13;
        public const int REG_ERR_UNKNOWN = 14;

    	readonly char[] delimit = { '\x005c' };  //Hex for '\'

    	public int WriteToReg(RegutilKeys Key, string RegPath, string KeyName, object KeyValue)
        {
    		string[] RegString = RegPath.Split(delimit);

            //First item of array will be the base key,return REG_ERR_SECURITY; so be carefull iterating below
            var RegKey = new RegistryKey[RegPath.Length + 1];

            //Returns proper key --  Will Default to hkey_current_user
            RegKey[0] = SelectKey(Key);

            for (int i = 0; i < RegString.Length; i++)
            {

                RegKey[i + 1] = RegKey[i].OpenSubKey(RegString[i], true);
                //If key does not exist, create it.  This logic usually suits my needs, but 
                //you may change it if you wish.
                if (RegKey[i + 1] == null)
                {
                    RegKey[i + 1] = RegKey[i].CreateSubKey(RegString[i]);
                }
            }
            try
            {
                //Write the value to the registry.  If fail, return the constant values defined at the beginning of the class
                RegKey[RegString.Length].SetValue(KeyName, KeyValue);
            }
            catch (System.NullReferenceException)
            {
                return REG_ERR_NULL_REFERENCE;
            }
            catch (System.UnauthorizedAccessException)
            {

                return REG_ERR_SECURITY;
            }

            return 0;
        }//-----------  End WriteToReg  -----------------------------------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Key"> </param>
        /// <param name="RegPath"> </param>
        /// <param name="KeyName"> </param>
        /// <param name="DefaultValue"> </param>
        public object ReadFromRegistry(RegutilKeys Key, string RegPath, string KeyName, object DefaultValue)
        {
        	object result = null;
            string[] regString = RegPath.Split(delimit);
            //First item of array will be the base key, so be carefull iterating below
            RegistryKey[] RegKey = new RegistryKey[RegPath.Length + 1];
            //Returns proper key --  Will Default to HKEY_CURRENT_USER
            RegKey[0] = SelectKey(Key);
            for (int i = 0; i < regString.Length; i++)
            {
                RegKey[i + 1] = RegKey[i].OpenSubKey(regString[i]);
                if (RegKey[i + 1] == null) //&& i < RegString.Length - 1 ) {
                    break;

                if (i == regString.Length - 1)
                {
                    result = (object)RegKey[i + 1].GetValue(KeyName, DefaultValue);
                }
            }
            if (result == null)
                result = DefaultValue;
            return result;
        }  //  --------------  End ReadFromRegistry  --------------------------------------------------------------------------- 


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Key"> </param>
        /// <param name="RegPath"> </param>
        public ArrayList GetEnumValFromRegistry(RegutilKeys Key, string RegPath)
        {
        	string[] regString = RegPath.Split(delimit);
            var retValue = new ArrayList();

            var regKey = new RegistryKey[RegPath.Length + 1];
            regKey[0] = SelectKey(Key);

            for (int i = 0; i < regString.Length; i++)
            {
                regKey[i + 1] = regKey[i].OpenSubKey(regString[i]); // zwraca null jeśli takiego klucza nie ma
                if (regKey[i + 1] == null)
                    break;
            	if (i != regString.Length - 1) continue;
            	string[] s1 = regKey[i + 1].GetValueNames();
            	for (int j = 0; j < s1.Length; j++)
            		retValue.Add(regKey[i + 1].GetValue(s1[j]));
            }

            return retValue;
        }  
		

        //Separated for cleanliness
        private RegistryKey SelectKey(RegutilKeys Key)
        {
            switch (Key)
            {
                case RegutilKeys.HKCR:
                    return Registry.ClassesRoot;
                case RegutilKeys.HKCU:
                    return Registry.CurrentUser;
                case RegutilKeys.HKLM:
                    return Registry.LocalMachine;
                case RegutilKeys.HKU:
                    return Registry.Users;
                case RegutilKeys.HKCC:
                    return Registry.CurrentConfig;
                default:
                    return Registry.CurrentUser;
            }
        }
    }//  end Class 

}
