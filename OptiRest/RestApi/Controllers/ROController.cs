﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;

namespace RestApi.Controllers
{
    
    public class RoController : ApiController
    {
        private readonly IDokumentyService _dokumentyService;

        public RoController(IDokumentyService dokumentyService)
        {
            _dokumentyService = dokumentyService ?? throw new ArgumentNullException(nameof(dokumentyService));
        }

          
        [HttpPost]
        public DokumentDto AddRo([FromBody]ImportDokumentDto impTranDto)
        {
            return _dokumentyService.DodajDokumentRo(impTranDto);
        }
               
    }
}