﻿using System;
using System.Collections.Generic;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;

namespace com.asoft.OptimaLib.Service
{
    public interface ITowaryService
    {

        List<TowaryDto> ListaTowarow(TowaryKryteriaDto towaryKryteriaDto);
        List<ZasobyDto> ListaZasobow(ZasobyKryteriaDto zasobyKryteriaDto);
    }
}