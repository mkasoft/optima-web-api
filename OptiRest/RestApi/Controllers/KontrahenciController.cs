﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using RestApi.App_Start;

namespace RestApi.Controllers
{
    
    public class KontrahenciController : ApiController
    {
        private readonly IKontrahenciService _kontrehenciService;

        public KontrahenciController(IKontrahenciService kontrahenciService)
        {
            _kontrehenciService = kontrahenciService ?? throw new ArgumentNullException(nameof(kontrahenciService));
        }


        [HttpGet]
        [System.Web.Http.Route("api/ListaKontrahentowPoDacie")]
        public List<KontrahentDto> ListaKontrahentowPoDacie([FromUri]string data)
        {
            return _kontrehenciService.ListaKontrahentow(data);
        }
       
        [HttpGet]
        [System.Web.Http.Route("api/ListaKontrahentow")]
        public List<KontrahentDto> ListaKontrahentow()
        {
            return _kontrehenciService.ListaKontrahentow();
        }

    }
}