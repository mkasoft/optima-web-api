﻿using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using System;

namespace com.asoft.OptimaLib.Service
{
    public class OptimaDokumentException : Exception
        
    {
        
        public int ErrorNumber { get; set; }
        public DokumentyKryteriaDto CriteriaDto { get; set; }
        public ImportDokumentDto ImpTranDto { get; set; }
        
        public OptimaDokumentException()
        {
        }

        public OptimaDokumentException(string message, ImportDokumentDto impTranDto, Exception inner)
            : base(message, inner)
        {
            ImpTranDto = impTranDto;
        }


        public OptimaDokumentException(int errorNumber, string message, DokumentyKryteriaDto criteriaDto, Exception inner)
     : base(message, inner)
        {
            ErrorNumber = errorNumber;
            CriteriaDto = criteriaDto;
        }
        public OptimaDokumentException(int errorNumber,string message, ImportDokumentDto impTranDto,Exception inner)
           : base(message, inner)
        {
            ErrorNumber = errorNumber;
            ImpTranDto = impTranDto;
        }

        public OptimaDokumentException(int errorNumber, string message, Exception inner)
            : base(message, inner)
        {
            ErrorNumber = errorNumber;
        }
        public OptimaDokumentException( string message, Exception inner)
            : base(message, inner)
        {
        }

        public string GetPayloadError()
        {
            if (ImpTranDto != null)
            {
                return ImpTranDto.ToExceptionString();
            }
            else if (CriteriaDto != null)
            {
                return CriteriaDto.ToExceptionString();
            }

            return "";
        }
        
    }
}


