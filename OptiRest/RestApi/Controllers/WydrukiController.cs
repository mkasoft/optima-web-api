﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using RestApi.App_Start;

namespace RestApi.Controllers
{
    
    public class WydrukiController : ApiController
    {
        private readonly IWydrukiService _wydrukiService;

        public WydrukiController(IWydrukiService wydrukiService)
        {
            _wydrukiService = wydrukiService ?? throw new ArgumentNullException(nameof(wydrukiService));
        }


        [Route("api/wydrukPdf")]
        [HttpGet]
        public string wydrukPdf([FromUri]long trnId)
        {
            string fileName = _wydrukiService.pdf(trnId);
            return fileName;
        }
        

    }
}