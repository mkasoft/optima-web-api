﻿using System;

namespace com.asoft.OptimaLib.dtoService
{
    public class TowaryDto
    {
      

       
        public long Id { get; set; }
        public string TowarKod { get; set; }
        public string TowarNazwa { get; set; }      
        public string JM { get; set; }

        public DateTime DataModyfikacji { get; set; }

        public string ToExceptionString()
        {
            return "Towar:" + " Id= " + Id + " TowarKod= " + TowarKod + " TowarNazwa =" + " " + TowarNazwa;
        }

        public override string ToString()
        {
            return "Towar:" + " Id= " + Id + " TowarKod= " + TowarKod + " TowarNazwa =" + " " + TowarNazwa;

        }
    }
}