﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using com.asoft.OptimaLib.dtoCriteria;
using com.asoft.OptimaLib.dtoService;
using com.asoft.OptimaLib.Service;
using Newtonsoft.Json;
using RestApi.App_Start;

namespace RestApi.Controllers
{
    
    public class TowaryController : ApiController
    {
        private readonly ITowaryService _towaryService;

        public TowaryController(ITowaryService towaryService)
        {
            _towaryService = towaryService ?? throw new ArgumentNullException(nameof(towaryService));
        }


        [Route("api/ListaTowarow")]
        [HttpGet]
        public List<TowaryDto> ListaTowarowPoDaciePoDacie([FromUri]TowaryKryteriaDto towaryKryteriaDto)
        {
            return _towaryService.ListaTowarow(towaryKryteriaDto);
        }

        [Route("api/ListaZasobow")]
        [HttpGet]
        public List<ZasobyDto> ListaZasobow([FromUri]ZasobyKryteriaDto zasobyKryteriaDto)
        {
            return _towaryService.ListaZasobow(zasobyKryteriaDto);
        }


    }
}