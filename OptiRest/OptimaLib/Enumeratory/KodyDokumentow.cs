﻿namespace com.asoft.OptimaLib.tools
{
    public class KodyDokumentowHermes
    {
 
        public static readonly string PA = "010";
        public static readonly string PAK = "011";
        public static readonly string FS = "020";
        public static readonly string FSK = "021";
        public static readonly string WZS = "031";
        public static readonly string WZ = "031";
        public static readonly string PZ = "037";
        
    }

}