﻿using System.Net; 
using System.Net.Http;  
using System.Web.Http.Filters;
using log4net;
using RestApi.App_Start;

namespace RestApi
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        ILog log = log4net.LogManager.GetLogger(typeof(CustomExceptionFilter));

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            log.Error(actionExecutedContext.Exception.Message);
            //We can log this exception message to the file or database.  
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(actionExecutedContext.Exception.GetaAllMessages(actionExecutedContext.Exception.Message)),  
                ReasonPhrase = "Internal Server Error.Please Contact your Administrator."
            };
            actionExecutedContext.Response = response;
        }
    }
}