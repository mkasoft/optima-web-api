﻿using System;

namespace com.asoft.OptimaLib.dtoService
{
    public class ZasobyDto
    {
        public string Mag_Symbol { get; set; }
        public long Twr_TwrId { get; set; }
        public int Twr_Typ { get; set; }
        public int Twr_Produkt { get; set; }
        public string Twr_Kod { get; set; }
        public string Twr_NumerKat { get; set; }
        public string Twr_Nazwa { get; set; }
        public long Twr_KatId { get; set; }
        public string Twr_Kategoria { get; set; }
        public long Twr_KatZakId { get; set; }
        public string Twr_Opis { get; set; }
        public string Twr_JM { get; set; }
        public string Twr_JMZ { get; set; }
        public Int32 Twr_TwCNumer { get; set; }
        public long TwI_MagId { get; set; }
        public DateTime TwI_Data { get; set; }
        public decimal TwI_Ilosc { get; set; }
        public decimal TwI_Braki { get; set; }
        public decimal TwI_Rezerwacje { get; set; }
        public decimal TwI_Zamowienia { get; set; }
        public decimal TwI_Wartosc { get; set; }
        public long TwI_TwIId { get; set; }

        public  string ToExceptionString()
        {
            return "Towar:" + " Id= " + Twr_TwrId + " TowarKod= " + Twr_Kod + " TowarNazwa =" + " " + Twr_Nazwa;

        }

        public override string ToString()
        {
            return "Towar:" + " Id= " + Twr_TwrId + " TowarKod= " + Twr_Kod + " TowarNazwa =" + " " + Twr_Nazwa;

        }
    }
}